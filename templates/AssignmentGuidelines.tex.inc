\section*{Submission Guidelines}
\begin{itemize}
\item All work must be submitted before the lecture (13:30) on the due date above. 
\item Submission consists of creating a tag in your personal course git repository with format \texttt{your-handle/assignment-name}. For example, my homework for week 2 of the course would be available by checking out the tag \texttt{psanan/assignment-2} from my personal repository.
\end{itemize}
