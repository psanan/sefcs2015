\documentclass{beamer}

\usetheme{Darmstadt}
\setbeamertemplate{navigation symbols}{} % Remove the navigation symbols
\setbeamercovered{transparent}
\AtBeginSection{\frame{\sectionpage}}

%% Code Listings and other packages  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{../templates/Packages.tex.inc}

%% Common Title Page info %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\author{Patrick Sanan}
\institute[USI Lugano] 
{
USI Lugano \\ 
\medskip
\textit{patrick.sanan@usi.ch}
}
