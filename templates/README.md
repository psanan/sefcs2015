LectureTemplate.tex gives a template to base weekly lecture slides upon.

Note that it depends on an include of the file Lecture.tex.inc in this directory, 
so will only work in sibling directories of this one.
