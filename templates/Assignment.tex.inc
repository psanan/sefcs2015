\documentclass[11pt]{article}

\input{../templates/Packages.tex.inc}

\usepackage[colorlinks=true]{hyperref}

\author{Patrick Sanan [\href{mailto:patrick.sanan@usi.ch}{patrick.sanan@usi.ch}]}
