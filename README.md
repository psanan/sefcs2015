# Software Engineering for Computational Science
Wednesdays 13:30-15:15, SI-013
EXCEPT Tuesday 13:30-15:15 Nov 10 [Week 9]

September 16 - December 16, 2015.

### Lecturer : Dr. Patrick Sanan 
 - @psanan
 - Lab 2nd floor
 - Office hours: by appointment

### Teaching Assistant : Patrick Zulian 
 - @zulianp
 - Office Hours: By appointment, every Monday from 10:00 AM to 11:30 AM 

### Links
https://www2.icorsi.ch/course/view.php?id=4302

### Assignments
Assignments are included with each week's material in this repository. To access them, you can clone this repository to your local machine

    git clone https://bitbucket.org/psanan/sefcs2015.git

and look at the assignments there, for example for week 1

    cd sefcs2015
    git pull
    cd week1
    open Assignment1.pdf

 Alternately, download the assignment pdf from Bitbucket. For example, click on the "Source" icon on the left, navigate to "week1", click on "Assignment1.pdf" and select "View raw"

## Books (none required)
### C
- Brian Kernighan and Dennis Ritchie, "The C Programming Language"
### C++
- Bjarne Stroustrop, "The C++ Programming Language". 
- Scott Meyers, "Effective C++" and "Effective STL"
### Git
- The Git Book: https://git-scm.com/book/en/v2

## Goals for this Course
The overarching aim is to prepare a practical foundation for continued work with software in computational science. Thus, we will cover

- Basic C and C++
- Basic usage of the command-line and many useful tools there
- The use of version control and other best practices for software design

We will *not* cover the use of high-level tools or GUIs, though of course these are commonly used by software engineers. Understanding the low level tools we introduce will make it easy to transition to these more elaborate tools later.

We will prioritize techniques relevant to computational science, such as testing, and deemphasize some topics which are important for commercial software engineers (such as working with complicated release schedules or truly massive codebases).

This is a small class, which means that we have the luxury of an interactive environment. The instructors cannot perfectly predict what you will struggle with, so feedback is essential.

## Before the First Lecture
So that we can begin efficiently, please come to the first lecture with your laptop set up  with :

- A terminal emulator which can run the bash shell
- Essential command-line tools (ssh, gcc, make, etc)
- A package manager

This should not require any additional setup with linux. If using OS X, you can follow the instructions here to install XCode (and the developer command-line tools) and MacPorts: https://www.macports.org/install.php . If you already use homebrew or some other system, feel free to continue using it (and note that it's not a good idea to attempt to use both homebrew and macports).

## Guidelines
Some simple points to keep in mind as you work in this course. 

Remember: **it should be fun!** If you’re frustrated, try to figure out how your workflow is wrong.

### Plan ahead. 
- Choose good abstractions.
- Consider top-down design.
- Consider test-driven design.
- Practice defensive coding : plan for you and your users to make errors.
- "The easiest bug to find is the one you didn't introduce."
- Take the time to understand concepts that you know are causing you to waste time.
### Don’t be afraid to rewrite and refactor.
- Code is often better-commented the second time.
- Code is often less buggy the second time.
- Code is often more efficient the second time.
- Code is often better-organized the second time.
### Use version control for everything.
- Modern tools make this very easy and lightweight. In git,
    1. `git init`
    2. `git add file1 file2`
    3. `git commit -m"Initial Commit"`
### Test often.
- Test early
- Separate what should work from what provably works.
### Write to be read. 
- Comment and document.
- Be consistent.
- Strive for self-documenting code.
- Consider your reader (even if it's just you).
- Again, don't be afraid to rewrite.
### Learn how to ask for help effectively.
(See Lecture 3)
#### Formulating Questions
#### Mailing Lists
#### Message Boards / Q+A sites

## Policies
- You will have an assignment each week, due before the next lecture.
- No late work will be accepted without approval from the instructors *before the deadline*. However, we will drop your lowest score [and possibly your lowest N scores].
- The assignments will be based around working towards building your own application.
- Each student will maintain their own private repository on bitbucket, which the instructor and TA will have read access to. Assignments will be submitted using git tags.
- Both correctness and organization of submitted code will be considered.
- Plagiarism
    - You will not learn from this course if you don't write your own code, so we expect anything submitted as your own work to be entirely written by you.
    - The following are allowed:
        - Discussing with your classmates about the general way a problem is solved
        - Asking and answering specific programming questions
    - The following are not allowed:
        - sending anyone else your code or giving another student access to your course git repository.
        - copying someone else’s code verbatim, or with trivial refactoring.
        - copying code snippets from the internet (StackOverflow, etc.) without citation. (Copying with citation is not cheating, but you will of course not receive credit for having written this code).
        - looking at someone else’s screen and transcribing their code 
    - From experience, it is *easy* to detect when people cheat. If we’ve done our job, it will be easier to do the work than to cheat well enough to fool us.

## Schedule [Tentative]
This will be a small course, so we can adapt the schedule if needbe. There will be 14 90-minute lectures, each including a section on C/C++ and a section on tools and design. There will be a short break between the two sections.

### Lecture 1:
#### Tools: The very basics (enough to compile and run C). 
- Bash, the environment, and basic commands
- Login files
- Extremely basic git/bitbucket
#### Programming: C
- Program structure
- Types
- Basic compilation
#### Assignment
See week1/Assignment.pdf
### Lecture 2
#### Programming: C Part 2. 
- Structs (and the need for classes)
- Pointers and arrays
- Dynamic memory allocation
- (C-style) strings
- Memory leaks
#### Tools
- Unix: piping, stdout, redirection
- The gdb (or lldb) debugger [No print statements!]
- valgrind memcheck [Cf. memory leaks]
- Text editors
- Code across multiple files
### Lecture 3
#### Programming: C++
- Why C++? (differences from C and motivations)
- Header files and include guards 
- Types.
- References (vs. pointers)
- OOP concepts (more later)
- Basic object-oriented programming (class vs. struct)	
- Streams
#### Tools
- Makefiles
- more unix tools (grep, find, du, ..)
- Process management 
- How to ask for help
    - Formulating intelligent questions
    - Mailing lists
    - Stackoverflow and other Q+A/message board sites
### Lecture 4
#### Programming: C++ 
- Style
    - Comments and #ifdef 0
    - variable naming 
    - Choosing a standard and sticking to it. [We won't enforce a specific one, but we will ask that you be consistent]
#### Tools: 
- More git
- Remote tools
### Lecture 5
#### Programming: C++: OOP (Given by Patrick Zulian)
- Inheritance
- Virtual functions
- Style
    - class definition layout
    - member variable naming
- Copy and assignment constructors
- In class exercise (bring your laptop)
#### Tools
- debuggers and C++
### Lecture 6
#### Programming: C++ 
- STL and data structures
- (STL) strings
- Namespaces
#### Tools: 
- Linking and libraries
    -link order
    -how dynamic/shared libraries work
    -nm, ldd
- Cross-compiling
- name-mangling
### Lecture 7
#### Programming: C++ (Given by Patrick Zulian)
- Design principles
- Design patterns
- In class exercise (bring your laptop)
### Lecture 8 
[no lecture]
### Lecture 9
#### C++ Topics
- Casting
- Const correctness
- Assertions
- Exceptions
### Lecture 10
#### Test Driven Design for Numerical Computing
- Test Driven Design
- Testing numerical code
#### Prototyping and Wrapping in high level environments
- Prototyping
- Python
- Octave/MATLAB/Julia/SciPy(inc. matplotlib)
### Lecture 11
#### Advanced Git
- Dangerous Git: rebasing, rewriting, etc.
### Applications and Distribution
- Application building
    - APIs and ABIs
    - (Semantic) Versioning
    - Documentation
### Lecture 12
#### Refactoring and Rewriting
#### Tools: More Application building
- Cmake and build systems
### Lecture 13
#### Programming: Advanced C++ 
- Constructors and the rule of three (or five)
- templates
    - typename
- Shared pointers / smart pointers
- Static polymorphism, with CRTP
#### Git Workflows
#### Final preparation
### Lecture 14
#### Programming: Advanced C++ 
- C++11 threads
- Other C++11 features
     - auto
     - move semantics
     - delete
     - lambdas
     - default
- New things in C++14 to be aware of
- Compiler-specific tools
    - Intel compiler pragmas
    - gcc attributes
    - ..
#### Tools
- profiling
- Debugging multithreaded code [brief]
- OS X topics
#### Final 
- Brief in-class presentations