/* 
  This program computes an array of values 1 ... ARRAY_SIZE
   in an inefficent way. The output should be:
  1   1   1   1   1   1   1   1   1
  1   1   1   1   1   1   1   1   9
  1   1   1   1   1   1   1   8   9
  1   1   1   1   1   1   7   8   9
  1   1   1   1   1   6   7   8   9
  1   1   1   1   5   6   7   8   9
  1   1   1   4   5   6   7   8   9
  1   1   3   4   5   6   7   8   9
  1   2   3   4   5   6   7   8   9

 Compile with: 
  gcc -Wall -g -std=c99 broken.c -o runme

 Run with:
  ./runme

 */
#include <stdlib.h>

int main(int argc, char *argv[])
{
  int *a; 

  const int kArraySize = 9;

  /* Initialize the array to values of 1 */
  a = malloc(kArraySize);
  for (int i=0; i<kArraySize; ++i){ 
    a[i] = 1; 
  }

  /* Print the original array */
  PrintArray(a,kArraySize);

  /* For each entry, starting from the back of the array,
     add the sum of all preceding entries */
  for (int i=kArraySize; i>0; --i){
    for (int j=0; j<i; ++i){
      a[i] += a[j];
    }
    PrintArray(a,kArraySize);
  }

  return 0;
}

static void PrintArray(int a,int n)
{
 for(int i=0; i<n; ++i){
  printf("%3.0d ",a[i]);
 }
 printf("\n");

 return 0;
}
