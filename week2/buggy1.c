#include<stdio.h>
#include<string.h>

int main(int argc, char *argv[]){
  char str[] = "Hello, World!"; 
  size_t len = strlen(str);
  
  printf("Original string: %s\n",str);

  for(int i=0;i<len/2;++i){
    const int j = len-i;
    const char temp = str[i];
    str[i] = str[j];
    str[j] = temp;
  }

  printf("Reversed string: %s\n",str);

  return 0;
}
