\input{"../templates/Lecture.tex.inc"}

\title[CEFCS Week 2]{Software Engineering for Computational Science: Week 2} 
\subtitle{C Arrays and Memory management, Debugging C}
\date{September 23, 2015} 

\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\titlepage 
\end{frame}

\begin{frame}
\tableofcontents 
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Programming: C, continued}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Arrays and Memory Management}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Arrays and Pointers in C}
\begin{itemize}
\item C programming often uses a low level abstraction of a series of equally-spaced locations in memory
\item These locations are numbered, typically by the byte (8 bits)
\item A variable of a given type occupies one or more of these locations. The \lstinline{&} operator returns the memory address used to store a variable's value
\begin{lstlisting}[language=C++]
int a = 42;
printf("The value of a is %d, and the address of a is %p\n",a,&a);
\end{lstlisting}
\item A \emph{pointer} is just a type of variable which holds a memory address.
\begin{lstlisting}[language=C++]
int an_int;
int *pointer_to_an_int;
int* also_pointer_to_an_int;
\end{lstlisting}
\end{itemize}
\end{frame}


\begin{frame}[fragile]
\frametitle{Arrays and Pointers in C}
\begin{itemize}
\item Pointers can be \emph{dereferenced} to obtain the value in the memory address ``pointed to''
\begin{lstlisting}[language=C++]
int an_int;
int *pointer_to_an_int
\end{lstlisting}
\item Style: \lstinline{int* a} or \lstinline{int *a}?
\begin{itemize}
\item \lstinline{int *a}: less chance of error when declaring multiple variables \lstinline{int *a,*b}.
\item \lstinline{int* a}: better expresses that the type of \lstinline{a} is ``pointer to an int''.
\item The important thing, as with most matters of style, is \emph{consistency}.
\end{itemize}
\end{itemize}

\end{frame}
\begin{frame}[fragile]
\frametitle{Arrays and Pointers in C}

\begin{itemize}
\item An \emph{array} is a type of variable which represents a series of values of some time, contiguous in memory.
\item An \emph{array name} in C, like a pointer, also describes a memory location. The location cannot be manipulated by the programmer.
\item Arrays can be fixed-sized (allocated on the \emph{stack})
\begin{lstlisting}[language=C++]
int a[5]; //an array of 5 ints
\end{lstlisting}
\item In both cases, array elements are accessed beginning with \lstinline{0}.
\begin{lstlisting}[language=C++]
a = int[5];
printf("The first element is %d\n",a[0]);
printf("The fifth element is %d\n",a[4]); //or is it *love*?
printf("The sixth element is %d\n",a[5]); //runtime error (segmentation fault)
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\begin{itemize}
\item Variable-sized arrays can be used with pointer and memory allocated on the \emph{heap}. 
\item You must alloc and free the memory required yourself.
\item The size is kept track of in an implementation-specific way and you (unfortunately) do not have access to this information as a programmer.
\item The main tool for doing this is \lstinline{malloc}, found in \texttt{stdlib.h}. Its argument is a number of bytes, so the \lstinline{sizeof()} function is useful.
\item We also introduce the concept of a \emph{cast} here, since malloc returns a value of type \lstinline{void *}.
\begin{lstlisting}[language=C++]
int *a;
int N = 10;
a = (int*) malloc(N * sizeof(int));
\end{lstlisting}
\item All calls to \lstinline{malloc} must be matched with calls to \lstinline{free}. Failure to do so results in a \emph{memory leak}, where memory is allocated and never released.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Equivalence between Arrays and Pointers}
\begin{itemize}
\item In most ways, arrays and pointers are the same
\begin{itemize}
\item Both can be dereferenced
\item Both can be used with array access index
\item You can even assign an array name to a pointer
\begin{lstlisting}[language=C++]
int *a;
int b[];
a = b;
\end{lstlisting}
\item You can pass either type to a function expecting a pointer. (More on functions later)
\end{itemize}
\item How are they different, then?
\begin{itemize}
\item The key difference is that you cannot change the memory location of an array, but you can change the value of a pointer
\item This makes arrays slightly more efficent, if you can use one.
% Put fun command to show assembly if time
\item When passing to a function, array names are converted to pointers.
\item The C99 standard also introduced ``variable-length arrays'', which we will not address.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Strings}
\begin{itemize}
\item Strings in C are null-terminated arrays of characters
\item ``null-terminated'' just means that the last character is \lstinline{\0} (``null'')
\item This allows for various functions to operate on strings and know when they end
\item This also means that a string is one character longer that it appears. The following is an array of 4 characters representing a 3-character string.
\begin{lstlisting}[language=C++]
char str[] = "abc";
\end{lstlisting}
\item It's important to know how C strings work, but if you are working heavily with strings, consider C++'s \lstinline{std::string} instead.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Functions and Multiple Compilation Units}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Functions}
\begin{itemize}
\item We have already seen one function: the \lstinline{main} function.
\item In many ways, functions are like variables (they are \emph{identifiers})
\begin{itemize}
\item They reside in memory
\item They can be pointed to
\item They can be declared and defined
\end{itemize}
\item A \emph{function prototype} declares which parameters a function accepts and returns.
\begin{lstlisting}[language=C++]
int MyFunction(int a, float b);
\end{lstlisting}
\begin{itemize}
\item The variable names in a prototype are not required (but be consistent - either use them all the time or never).
\end{itemize}
\item A function definition specifies what will be compiled to produce code which can perform operations
\begin{lstlisting}[language=C++]
int MyFunction(int a, float b){
  int r = a + (int) b;
  return r;
}
\end{lstlisting}
\item A function must be declared or defined before it can be used in other code. 
\item A function must be defined (somewhere) before it can be used.
\end{itemize}

For more, see Chapter 4 of Kernighan and Ritchie.
\end{frame}

\begin{frame}[fragile]
\frametitle{Multiple Translation Units}
\begin{itemize}
\item Motivation
\begin{itemize}
\item Larger projects tend to be spread over several files for organization purposes.
\item It becomes slow to recompile all of a large program every time a change is made, so it is possible to compile parts of the program separately and \emph{link} them together into the final executable
\item These separate parts of the program are called \emph{objects}
\end{itemize}
\item A \emph{translation unit} (or \emph{compilation unit}, informally) is what is input to a compiler to produce an object file. This can be a single file, but it is common to use the C preprocessor to combine several files for compilation (this is all the \lstinline{#include} statements you have seen are doing).
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\begin{itemize}
\item With GCC, one instructs the compile to produce object files with the \lstinline{-c} flag:
\begin{lstlisting}[bash]
gcc -Wall -c -o mycode.o mycode.c
\end{lstlisting}
\item Once object files have been created, several can be \emph{linked} together
\begin{lstlisting}[language=C++]
gcc -Wall -o runme mycode.o morecode.o
\end{lstlisting}
\item You must define a \lstinline{main} function somewhere to produce a valid executable.
\item We will cover how to thoroughly debug linking errors in a later lecture.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{}
\begin{itemize}
\item Calling functions from other translation units is allowed as long as they are declared. This is the purpose of a header file like \lstinline{stdio.h}.
\item One can declare a function is \lstinline{static}, meaning it cannot be called from other translation units.
\begin{lstlisting}[language=C++]
static int OnlyUsedHere(int a);
\end{lstlisting}
\item static functions are useful for functions which only relate to other functions in the same unit.
\begin{lstlisting}[language=C++]
static void SubTask1(){ /* .. */};
static void SubTask2(){ /* .. */};
void MainTask(){
  SubTask1();
  SubTask2();
};
\end{lstlisting}
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Break}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Break}

Break/Exercise: try to write and compile a simple program which uses two source files, and help each other debug. How was the debugging process?
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Tools}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Debugging Standard Advice}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Debugging Standard Advice 1}
\begin{itemize}
\item Standard advice 1 : \textbf{Heed Compiler Warnings}
\item Use strict settings when compiling (\lstinline{-Wall} or \lstinline{-Wpedantic})
\item Do not ignore compiler warnings
\item Why?
\begin{itemize}
\item Warnings are very often bugs waiting to happen
\item If you get into the habit of ignoring warnings, you will miss some
\item A ``clean compile'' (no warnings or errors) is desirable, especially if you share your code.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Debugging Standard Advice 2}
\begin{itemize}
\item Standard advice 2 : \textbf{Avoid Print Statements for Debugging}
\item Why?
\begin{itemize}
\item The edit-compile-run-examine-edit-compile-run-examine... cycle is slow
\item Your code can quickly become cluttered and you can even introduce bugs while debugging.
\item You have to decide what quantities are of interest, compile, and then amend your assumptions
\item There are tools that make it easier, such as gdb/lldb
\item There are tools which can even check for bugs you haven't noticed (yet), such as valgrind
\end{itemize}
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Debugging with gdb and lldb}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\frametitle{GDB}
\begin{itemize}
\item GDB and related debuggers work with a table which maps instructions, memory locations, etc. in the compiled code to line numbers, function names, variable names, etc. in the source code.
\item GDB can then use the combination of an executable and this extra information to step through code, instruction by instruction, and allow the user to examine the values of variables, the execution path through the code, and can even allow the execution of functions.
\item To build the table with GCC, use the \lstinline{-g} flag
\begin{lstlisting}[language=C++]
gcc -Wall -g -o mycode.o mycode.c
\end{lstlisting}
\item We won't get into the details of this table until we need to, but we will mention a common pitfall: make sure that you still have the table if you want to debug!
\begin{itemize}
\item If you linked several object files into an executable and deleted them, you can run the program, but the symbol table is gone.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Differences between LLDB and GBD}
\begin{itemize}
\item For the purposes of our demonstations today, there should be none, but be aware that they are not identical!
\item For example, GDB includes a handy feature to examine several entries of an array, for example
\begin{lstlisting}[language=C++]
p my_array@4
\end{lstlisting}
\item The \lstinline{@} operator doesn't do this in LLDB, but a somewhat-awkward workaround is
\begin{lstlisting}[language=C++]
p *(int(*))[4])my_array
\end{lstlisting}
\item Sometimes, a better option is to call a simple viewing function (See example later on)
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{GBD/LLDB Example}
Check out the course repository if you don't already have it
\begin{lstlisting}[language=C++]
git clone https://bitbucket.org/psanan/cefcs2015
\end{lstlisting}
and compile \lstinline{week1/buggy1.c} with debugging symbols.
Does it behave as expected?
Use lldb or gdb:
\begin{lstlisting}[language=C++]
gdb ./myExecutable
b main
r
\end{lstlisting}
Use the \texttt{s} [step], \texttt{n} [next], \texttt{bt} [backtrace], and \texttt{p} [print] commands to locate the bug. (\texttt{q} to quit).
\end{frame}

\begin{frame}[fragile]
\frametitle{Solution}
Bug: On line 6, we forgot to add \lstinline{-1} to the computation of \lstinline{len} to account for the null-termination.\\
\scalebox{0.7}{
  \lstinputlisting[caption=\texttt{buggy1.c},numbers=left]{buggy1.c}
}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Debugging with Valgrind Memcheck}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Valgrind Memcheck}
\begin{itemize}
\item Valgrind is powerful set of tools for debugging and analysis
\item It contains several tools, but we will only consider the default one, \texttt{memcheck}, which attempts to detect illegal memory operations.
\item For much more, see the Valgrind website, \href{https://www.valgrind.org}{valgrind.org}.
\item To use it, simply prepend \lstinline{valgrind} to your execution command
\begin{lstlisting}[language=C++]
valgrind ./myprogram -option 1
\end{lstlisting}
\item The output will generally be long, but pay special attention to the summary (and instructions to rerun with \texttt{--leak-check=full} if needbe, and warnings of illegal operations of specific lines of your code.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Example}
\begin{itemize}
\item Build and debug the following code, \texttt{week2/buggy2.c} with gdb and/or valgrind memcheck.
\item Note that you can call the \texttt{PrintArray} function directly from GDB, for example \lstinline{call PrintArray(a,3)}
\end{itemize}
\scalebox{0.6}{
  \lstinputlisting[numbers=left]{buggy2.c}
}
\end{frame}

\begin{frame}[fragile]
\frametitle{Solution}
Note that on some setups, this (buggy!) code might run without throwing any errors. This is why tools like valgrind are very useful.
\begin{itemize}
\item Missing \lstinline{free(a)}
\item Missing \lstinline{sizeof(double)} on line 16

\end{itemize}
\scalebox{0.5}{
  \lstinputlisting[numbers=left]{buggy2.c}
}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Unix: streams and files}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Unix streams and files}
\begin{itemize}
\item A killer feature of Unix is the ability to compose processes and redirect input and output
\item A \emph{stream} is an abstraction which accepts or produces one character at a time
\item You are already familiar with the streams \texttt{stdout} and \texttt{stderr}, which are where the output and errors from your programs go to be displayed in the terminal
\item Pipes take the output stream of one command and make it the input stream of the next
\begin{lstlisting}[language=C++]
ls -a | grep *.txt
\end{lstlisting}
\item The \lstinline{>} operator directs a stream to a file
\begin{lstlisting}[language=C++]
ls > filelist.txt
\end{lstlisting}
\item \lstinline{tee} directs its input to a file and to stdout.
\item For many more, see \href{www.gnu.org/software/bash/manual/bash.html#Shell-Builtin-Commands}{The GNU Bash manual on Built-in commands}.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Text Editors}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Text Editors}
\begin{itemize}
\item It's important to know the basics of a text editor that is available everywhere. 
\item \lstinline{vi} (or its extension \lstinline{vim}) is available on essentially every system, and we highly recommend that you learn the basics. 
\item \lstinline{emacs} is another popular, widely available choice.
\item There exist some light-weight editors which you can use, such as \lstinline{pico}/\lstinline{nano} 
\item We will not assign any particlar requirements with respect to editors in this course, but we expect that you be able to edit text files from the terminal in some way.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Text Editors}
Exercise:
\begin{itemize}
\item open a new file in vi \lstinline{vi test.c}. You are in ``normal mode''. Keys execute commands, as opposed to inserting text
\item enter ``insert mode'' by typing \lstinline{i}. You can now insert text.
\item type \lstinline{ESC} to return to normal mode. Move around with the \lstinline{hjkl} keys (as with a video game. Use the arrow keys if you must!)
\item to save, type \lstinline{:w} from normal mode
\item to quit, type \lstinline{:q} from normal mode
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Assignment}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Lorenz 99 System}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\texttt{week2/Assignment2.pdf} in the course repository.
$$
\frac{dX_k}{dt} = -X_{k-2}X_{k-1} + X_{k-1}X_{k+1} - X_k + F
$$
\begin{itemize}
\item Make sure you have \texttt{valgrind} and a debugger (\texttt{lldb} or \texttt{gdb}) installed on your system. 
\item Suggested Reading
\begin{itemize}
\item Kernighan and Ritchie, Chapter 1. The rest of that book may be used as a reference.
\item \href{www.gnu.org/software/gdb/documentation}{GDB manual at gnu.org}
\item The Valgrind ``Quick start Guide'' at \texttt{valgrind.org}
\item The Built-in command section at \texttt{www.gnu.org/software/bash/manual/bash.html}
\end{itemize}
\end{itemize}
\end{frame}
\end{document}
