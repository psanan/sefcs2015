#include <stdio.h>
#include <stdlib.h>

static void PrintArray(double *a, int n)
{
 for(int i=0; i<n; ++i){
  printf("%g ",a[i]);
 }
 printf("\n");
}

int main(int argc, char *argv[])
{
  double *a;
  const int kSize =  10;
  a = (double*)malloc(kSize);
  for(int i=0; i<kSize; ++i){
    a[i] = 0.1 * i;
  }
  printf("We expect the numbers 0 0.1 0.2 ... 0.9 :\n");
  PrintArray(a,kSize);
  return 0;
}
