 //new overriden methods from fruit
 double Apple::getWeight() const override { return 11; }
 double Kiwi::getWeight() const override { return 10; }

 class Animal {
 public:	
	virtual void eat(const Fruit &fruit) {
		assert(fruit.getWeight() >= 0);  //pre-condition
		assert(fruit.getWeight() < 12);	 //pre-condition
		//Do stuff ...
		assert(!hungry()); //post-condition
	} 
	virtual bool hungry() const { /* Do stuff ...*/ }
	virtual Fruit * favoriteFruit() = 0;
	virtual ~Animal() {}
};

class Tiger : public Animal {
	void eat(const Fruit &fruit) override {
		assert(fruit.getWeight() >= 0);  //pre-condition
		assert(fruit.getWeight() < 10);  //pre-condition
		//Do stuff ...
		assert(hungry()); //post-condition
	} 

	void eat(const Apple &fruit) {	/* Do stuff */ } 
	virtual Apple * favoriteFruit() override { return new Apple(); } //!?
};

