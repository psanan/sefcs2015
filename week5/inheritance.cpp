class Fruit {
public:
    virtual std::string getName() const = 0;
    virtual ~Fruit() {}
};

class Apple : public Fruit {
public:
    inline std::string getName() const override { return "Apple"; }
};

class Kiwi : public Fruit {
public:
    inline std::string getName() const override { return "Kiwi"; }
};

//Compiles?
Fruit * fruit = new Kiwi(); //1
Apple * apple = fruit;      //2
fruit = new Apple();        //3
apple = fruit;              //4
apple = new Kiwi();         //5

//What is the output after we removed the line which do not compile?
std::cout << fruit->getName() << std::endl;