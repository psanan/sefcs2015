class Feline : public Animal {
    // Methods and more methods ....
private:
     VeryLargeClassMemeber vlcm_;
};

class Cheetah : public virtual Feline {
public: 
    virtual void eat(const Fruit &fruit) override {  /* Do stuff..*/ } //....
};

class MountainLion : public virtual Feline {
public: 
    virtual void eat(const Fruit &fruit) override {  /* Do stuff..*/ } //....
};

class WeirdFeline : public Cheetah, public MountainLion {
public: 
    //Choice of method mandatory
    void eat(const Fruit &fruit) override {  Cheetah::eat(fruit); } 
};

//What are the issues if any and how are they handled? 
//Is multiple inheritance good or bad? 
//What other alternative do we have?