
ClassWithPtr::ClassWithPtr()
: BaseClass(), n_elments_(0), some_ptr_(nullptr) {}

ClassWithPtr::ClassWithPtr(const ClassWithPtr &other) 
: BaseClass(other), n_elements_(other.n_elments_) {
    using std::copy;

    some_ptr_ = other.some_ptr_;  //Is this correct???

    //Or this is correct??
    some_ptr_ = new double[n_elments_];
    copy(other.some_ptr_, other.some_ptr_ + n_elments_, some_ptr_);
}

ClassWithPtr &ClassWithPtr::operator=(const ClassWithPtr &other) {
    using std::copy;

    if (this == &other) return *this; //Why this?

    BaseClass::operator=(other);
    if(n_elments_ != other.n_elments_) { //And why this?
        delete[] some_ptr_;
        n_elments_ = other.n_elments_;
        some_ptr_ = new double[n_elments_]
    }

    copy(other.some_ptr_, other.some_ptr_ + n_elments_, some_ptr_);
    return *this;
}

