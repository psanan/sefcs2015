
#include "App.hpp"
#include "Example.hpp"
#include "CallingContextTreeMonitor.hpp"

#include <stdlib.h>

int main(const int argc, const char * argv[])
{	
	using namespace sefcs;

	//make_shared and shared_ptr what are they for??
	using std::make_shared;

	if(argc > 1) {
		//check the argv for input
		std::string arg1 = argv[1];
	} else {
		//default monitor
		App::instance().set_monitor(make_shared<CallingContextTreeMonitor>());
	}


	Example example;
	example.method_a();
	example.method_d();
	
	std::vector<std::string> v(1, "HI");

	//Finalizing the app 
	App::instance().finalize();
	return EXIT_SUCCESS;
}


