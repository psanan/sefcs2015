#include "Example.hpp"
#include "App.hpp"


///__PRETTY_FUNCTION__ is a predefined preprocesor macro describing the signature of the method

namespace sefcs {
	void Example::method_a()
	{
		method_begin(__PRETTY_FUNCTION__);

		//call another method
		method_b();

		method_end(__PRETTY_FUNCTION__);
	}

	void Example::method_b()
	{
		method_begin(__PRETTY_FUNCTION__);

		//call another method N times
		for(int i = 0; i < 10; ++i) {
			method_c();
		}

		method_end(__PRETTY_FUNCTION__);
	}

	void Example::method_c()
	{
		method_begin(__PRETTY_FUNCTION__);

		method_end(__PRETTY_FUNCTION__);
	}

	void Example::method_d()
	{
		method_begin(__PRETTY_FUNCTION__);
		
		//call another method
		method_a();

		method_end(__PRETTY_FUNCTION__);
	}
}
