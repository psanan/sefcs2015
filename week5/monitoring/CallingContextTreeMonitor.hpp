#ifndef CALLING_CONTEXT_TREE_MONITOR_H
#define CALLING_CONTEXT_TREE_MONITOR_H 

#include "Monitor.hpp"
#include <memory>
#include <vector>

namespace sefcs {

	//ADD YOUR COMMENTS TO DOCUENT THE CLASS

	class CallingContextTreeMonitor : public Monitor {
	public:
		
		//Inner class: explain...
		class Invocation {
		public:
			Invocation(const std::string &name);
			~Invocation();
			void clear();

			void add_child(const std::shared_ptr<Invocation> &child);
			void set_parent(const std::shared_ptr<Invocation> &parent);
			std::shared_ptr<Invocation> get_parent();
			std::vector< std::shared_ptr<Invocation> > &get_children();



			const std::string &getName() const;

		private:
			std::string name_;
			std::vector< std::shared_ptr<Invocation> > children_;
			std::shared_ptr<Invocation> parent_;
		};


		void method_begin(const std::string &name) override;
		void method_end(const std::string &name) override;
		void monitoring_ended() override;

	private:
		void visit_print(const std::shared_ptr<Invocation> &invocation, const int level, std::ostream &os);
		std::shared_ptr<Invocation> tree_;
		std::vector< std::shared_ptr<Invocation> > forest_;
	};
}

#endif //CALLING_CONTEXT_TREE_MONITOR_H
