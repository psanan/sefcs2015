#include "CallingContextTreeMonitor.hpp"

#include <assert.h>
#include <iostream>

namespace sefcs {
	//For convenience we define a local type for the inner class
	typedef CallingContextTreeMonitor::Invocation InvocationT;

	InvocationT::Invocation(const std::string &name)
	: name_(name), parent_(nullptr) { }

	InvocationT::~Invocation()
	{
		// printf("~Invocation\n");
	}

	void InvocationT::clear()
	{
		for(auto child_ptr : children_) {
			child_ptr->clear();
		}

		children_.clear();
	}

	void InvocationT::add_child(const std::shared_ptr<InvocationT> &child) {
		children_.push_back(child);
	}

	void InvocationT::set_parent(const std::shared_ptr<InvocationT> &parent) {
		parent_ = parent;
	}

	std::shared_ptr<InvocationT> InvocationT::get_parent() {
		return parent_;
	}

	std::vector< std::shared_ptr<InvocationT> > & InvocationT::get_children()
	{
		return children_;
	}

	const std::string & InvocationT::getName() const
	{
		return name_;
	}

	void CallingContextTreeMonitor::method_begin(const std::string &name) {
		auto node = std::make_shared<InvocationT>(name);
		if(tree_) {
			tree_->add_child(node);
			node->set_parent(tree_);
			tree_ = node;
		} else {
			tree_ = node;
		}
	}

	void CallingContextTreeMonitor::method_end(const std::string &name) {
		assert(bool(tree_) && "tree_ should never be null!");
		(void) name; //unused


		if(!tree_) {
			std::cerr << "[Error] CallingContextTreeMonitor::method_end, this->tree_ is null" << std::endl;
			return;
		}

		if(!tree_->get_parent()) {
			forest_.push_back(tree_);
		}

		tree_ = tree_->get_parent();
	}

	void CallingContextTreeMonitor::monitoring_ended() {
		using std::shared_ptr;
		std::ostream &os = std::cout;

		if(forest_.empty()) {
			os << "Empty\n";
			return;
		}

		for(auto tree_ptr : forest_) {
			visit_print(tree_ptr, 0, os);
			//BONUS QUESTION: Why is this necessary to avoid memory leaks?
			tree_ptr->clear();
		}	

		forest_.clear();
	}

	void CallingContextTreeMonitor::visit_print(const std::shared_ptr<InvocationT> &invocation, const int level, std::ostream &os)
	{
		if(!invocation) return;
		

		for(int i = 0; i < level; ++i) {
			os << "     ";
		}

		if(level > 0) {
			os << "|--> ";
		}

		os << invocation->getName() << "\n";

		//depth first traversal 
		for(auto child_ptr : invocation->get_children()) {
			visit_print(child_ptr, level+1, os);
		}
	}
}



