#ifndef APP_HPP
#define APP_HPP 

#include "Monitor.hpp"
#include <memory>

namespace sefcs {
 	/**
 	 * Application singleton (only one instance exists)
 	 */
 	 class App {
 	 public:
 		///@return the instance of the App
 	 	static App &instance();

 		///@return the current monitor
 	 	inline std::shared_ptr<Monitor> monitor()
 	 	{
 	 		return monitor_;
 	 	}

 		///@param monitor the monitor begin set
 	 	inline void set_monitor(const std::shared_ptr<Monitor> &monitor)
 	 	{
 	 		monitor_ = monitor;
 	 	}

 		///To be called at the end of the application
 	 	void finalize();
 	 private:
 		///cannot be instantiate from outside
 	 	App();

 		///the current monitor
 	 	std::shared_ptr<Monitor> monitor_;
 	 };

 	//////////////////////////////////////////////////////////////////
 	//Convenience methods for intercepting method calls
 	//////////////////////////////////////////////////////////////////

 	/** To be called when the method begins 
 	 *	@param name is the name (or signature) of the method being called	
 	 */
 	 inline void method_begin(const std::string &name)
 	 {
 	 	auto m = App::instance().monitor();
 	 	if(m) m->method_begin(name);
 	 }

 	/** To be called when the method ends 
 	 *	@param name is the name (or signature) of the method being called	
 	 */
 	 inline void method_end(const std::string &name)
 	 {
 	 	auto m = App::instance().monitor();
 	 	if(m) m->method_end(name);
 	 }
 	}

#endif //APP_HPP
