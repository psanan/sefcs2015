
ClassWithPtr::ClassWithPtr(ClassWithPtr &&other) 
: BaseClass(std::move(other))
{
    delete[] some_ptr_;

    n_elments_ = other.n_elements_;
    //other.some_ptr_ is not need anymore hence we can steal it
    some_ptr_ = other.some_ptr_;

    //however we need to nullify the moved object
    other.some_ptr_ = nullptr; other.n_elments_ = 0;
}

ClassWithPtr &ClassWithPtr::operator=(ClassWithPtr &&other) {
    //if this happens the client is doing something really wrong
    assert(this != &other); 

    BaseClass::operator=(std::move(other));

    delete[] some_ptr_;
    n_elments_ = other.n_elements_;

    //other.some_ptr_ is not needed anymore, hence we can steal it
    some_ptr_ = other.some_ptr_;

    //however we need to nullify the moved object
    other.some_ptr_ = nullptr; other.n_elments_ = 0;
    return *this;
}
