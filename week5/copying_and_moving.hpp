 class ClassWithPtr : public BaseClass {
 public:	
 	//Empty default constructor can be stated as follows
 	// ClassWithPtr() = default; 

 	//However we need to handle the pointer
 	ClassWithPtr();

 	//copying
 	ClassWithPtr(const ClassWithPtr &other);
 	ClassWithPtr &operator=(const ClassWithPtr &other);

 	//moving
 	ClassWithPtr(ClassWithPtr &&other);
 	ClassWithPtr &operator=(ClassWithPtr &&other);

 	~ClassWithPtr();
 private:
 	int n_elments_;
 	double * some_ptr_; //or m_some_ptr
 };