
for n= [10] %[10,20]
    fprintf('\n\n\n*** n = %d ********** \n\n',n)
    N=[n, n, n, 0]; 
    dimens=[1/n 1/n 1/n];
    S=Simulator(N, dimens);
    S.assemble;
    S.solve;
    S.errors;
end