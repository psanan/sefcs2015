function [x,rnormhist,enormhist] = fcg(A,B,b,x0,xref,tol,~,~)
% FCG. 
%
% Note: We have followed poor Octave practice and not vectorized
%       the loops over k in the main loop below. This is for clarity
%       but changing this would be a simple optimization of this code
%
% Note: The preconditioner B is a function which is directly
%       applied to the residual, u = B(r). 
%
% Note: this function is for monitoring convergence rates
%       and analyzing data depencies (in this and extensions)
%       and is not intended to be an efficient implementation,
%       but a correct and hopefully easy-to-read one.
%
% Note: this function returns absolute residual norms.
%       If b has unit norm, these are suitable for analysis,
%       but otherwise normalization by norm(b) might be more useful

if(nargin<5)
  xref = NaN(size(b));
end
if(nargin<6)
  tol = 1e-10;
end

% In the interest of porting to a 0-based implementation, we
%   wrap all array accesses
idx = @(i) i+1;

% For analysis purposes, we start with no truncation
ndirs = @(i) i; %how many previous directions to use at iteration i

n = length(b);
maxit = n;
rnormhist = NaN(1,maxit); enormhist = rnormhist;

atol2 = tol * tol * norm(b);

beta = NaN(1,maxit);
eta  = NaN(1,maxit);
s    = NaN(n,maxit);
p    = NaN(n,maxit);

i            = 0;
x            = x0;
r            = b - A*x;
u            = B(r);
p(:,idx(i))  = u;
s(:,idx(i))  = A * p(:,idx(i));
gamma        = dot(r,u);
eta(idx(i))  = dot(s(:,idx(i)),p(:,idx(i)));
alpha        = gamma / eta(idx(i));

rnormhist(idx(i)) = norm(r); enormhist(idx(i)) = norm(x-xref);

for i = 1:maxit-1 % Note that these are 0-based iterations
  x            = x + alpha * p(:,idx(i-1));
  r            = r - alpha * s(:,idx(i-1));
  u            = B(r);
  gamma        = dot(r,u);

  for k = i-ndirs(i) : i-1
    beta(idx(k)) = -dot(u,s(:,idx(k)))/eta(idx(k)); %note sign
  end

  p(:,idx(i))    = u;
  for k = i-ndirs(i) : i-1
    p(:,idx(i))  = p(:,idx(i)) + beta(idx(k)) * p(:,idx(k)); %note sign
  end

  s(:,idx(i))    = A * p(:,idx(i));
  eta(idx(i))    = dot(s(:,idx(i)),p(:,idx(i)));
  alpha        = gamma / eta(idx(i));

  rnormhist(idx(i)) = norm(r); enormhist(idx(i)) = norm(x-xref);

  if(gamma < atol2)
    break;
  end
  
end

rnormhist = rnormhist(idx(0):idx(i)); enormhist = enormhist(idx(0):idx(i));
