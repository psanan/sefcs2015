\input{"../templates/Lecture.tex.inc"}

\title[CEFCS Week 10 ]{Software Engineering for Computational Science: Week 10} 
\subtitle{Prototyping and Testing}
\date{November 18,2015} 

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\titlepage 
\end{frame}

\begin{frame}
\tableofcontents 
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Prototyping}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\frametitle{Prototyping}
\begin{itemize}
\item C and C++ are generally not very useful for experimentation
\item If you are building ``toys'', consider using a different language
\item C++ is good at:
\begin{itemize}
\item Compiling into efficient code
\item Providing structures which scale to large code bases
\end{itemize}
\item However, it does not prioritize
\begin{itemize}
\item Compilation speed (or interactive execution)
\item Concise syntax
\item Standardized ``scoping''
\item Easily usable modules
\end{itemize}
\item It is worthwhile to learn to use one or two other languages/environments. 
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Alternative Environments}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\frametitle{Alternative Environments}
\begin{itemize}
\item ``Matrix Laboratories''
\begin{itemize}
\item MATLAB*\^{}
\item Octave
\item SciPy (+matplotlib)
\item Julia
\item ...
\end{itemize}
\item Computer Algebra Systems (CASs)
\begin{itemize}
\item Mathematica*\^{}
\item SAGE
\item SymPy
\item Maple*
\item ...
\end{itemize}
\end{itemize}
{\small
  * Not free/open source\\
  \^{} For more, see a course I taught previously on MATLAB and Mathematica, also on \texttt{bitbucket.org/psanan}
}

\end{frame}

\begin{frame}[t,fragile]
\frametitle{Learning to Use Prototyping Environments}
\begin{columns}
\column{0.5\textwidth}
\textbf{C++}
\begin{itemize}
\item Learn as many details as you can to gain efficiency
\item Learn efficient patterns
\item Practice forward-looking design and programming practices
\item Aim for well-structured, efficient code
\item Code should be extensible and reusable by someone who knows C++ well
\end{itemize}
\vfill
\column{0.5\textwidth}
\textbf{Prototyping Environments}
\begin{itemize}
\item Don't worry about the details until you need to
\item Learn tricks and anti-patterns to avoid
\item Practice easily-modified and read design and programming practices
\item Aim for optimized experimentation
\item Code should be understandable to another person, whether or not they're an expert in that language
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}[fragile]
\frametitle{Example - Krylov solver in Octave}
\begin{itemize}
\item 
See the included \texttt{week10/fcg.m}
\item A prototype implementation of the Flexible Conjugate Gradient (FCG) method for solving linear systems.
\item Compared to the C++ implementation that we wrote later,
\begin{itemize}
\item It is very inefficient. No attempt is made to reuse computed values or only allocate the minimum amount of memeroy
\item It computes extra information, here actual error and residual norms
\item It uses a trick to allow us to use 0-based indexing in MATLAB/Octave
\item Is easy to use interactively
\end{itemize}
\item The real implementation in PETSc: \texttt{https://bitbucket.org/petsc/petsc/}, see \texttt{src/ksp/ksp/impls/fcg/fcg.c}
\begin{itemize}
\item Interfaces with a powerful library
\item Optimizations, supports many options,
\item Much harder to read and modify!
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{When are you finished protoyping?}
\begin{itemize}
\item You know what the algorithm is
\item Your time is being wasted on things which you cannot easily optimize in your prototyping environment. You need to scale.
\item You need to use a library or infrastructure that does not support your prototyping environment.
\item Your project is scaling to the point of needing more powerful software engineering capabilities, such as those offered in C++
\item Note that some modern environments, particularly Julia, allow you to produce efficient code for many applications without needing to rewrite. This is clearly a useful property, but don't be too afraid of rewriting - rewriting improves your code immensely.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Testing}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\frametitle{Types of Tests (1/2)}
\begin{itemize}
\item There are many different types of tests you will hear about. A given test can fall into more than one definition. 
\item Some tests are mainly defined by their purpose:
\begin{itemize}
\item \textbf{Smoke tests} or \textbf{Sanity tests} : Very simple tests to answer questions like ``does anything run at all?'', ``should I bother running more tests?'' Essential for working with HPC systems.
\item \textbf{Regression Tests} : Tests maintained to confirm that existing functionality behaves as expected. ``Did I break anything?''. Particularly useful for refactoring and optimization.
\item \textbf{Acceptance Test}: Tests to determine if a piece of code has fulfilled its contract/requirements to a ``user''.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Types of Tests (2/2)}
\begin{itemize}
\item Some tests are mainly defined by what sort of code they test:
\begin{itemize}
\item \textbf{Unit Tests} ``Do the smallest testable units work?''
\item \textbf{Integration Tests} ``Do the units work together?''
\item \textbf{System Tests} ``Does the whole system work?''
\end{itemize}
\item Some are defined by the software development cycle itself (\textbf{alpha} and \textbf{beta} tests) but we will not concern ourselves with these.
\item In numerical computing, we might add some more categories of tests (not as standardized):
\begin{itemize}
\item \textbf{Convergence test} As some parameter $h\to 0$, does some error go to zero with the required asymptotic rate?
\item \textbf{Scaling test} As some parameter $N \to \infty$, does the time to solution of a problem (parameterized by $h$, perhaps) (or usage of other resources) behave as expected?
\item \textbf{Well-posedness test} is the solution map well-conditioned?
\item \textbf{Robustness test} is the parameters-to-solution map well-conditioned?
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Unit Tests}
\begin{itemize}
\item A ``Unit'' is generally thought of as the smallest testable part of an application
\item It usually tests a single logical concept
\item In numerical computing, these might be routines to compute norms, to read or write files, to compute a particular function, etc.
\item An example (from my PhD thesis work) of a unit test to check a linear algebra routine:
\begin{lstlisting}[language=C++,
basicstyle=\tiny\ttfamily]
  {
    cout << " --- 3x3 NRD check (one near-zero s.v.) --- " << endl;
    Matrix3r F,R,Y;
    F << 
      0.825181547872221,  -0.311240128055370,  -0.133807885798537,
      -0.301594344202202,  -1.093063840554562,  -0.017909030692390,
      -0.525197373026757,  -0.499119033277996,   0.046563327096107;
    nearestRotationDecompose(F,R,Y);
    cout << "det(R) = 1 :\t"; zeroCheckPrint(R.determinant()-1.0);
    cout << "R orthogonal :\t"; zeroCheckPrint( (R.transpose()*R - Matrix3r::Identity()).norm());
    cout << "residual :\t"; zeroCheckPrint((F - R*Y).norm());
  }
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Why Bother? (1/3)}
\begin{itemize}
\item Testing is no substitute for language and applied mathematics skills, but those alone will not suffice in realistic situations.
\item You will need regression tests
\item Testable code is easy to share. Other code is almost impossible to.
\item Test-driven design is its own reward
\begin{itemize}
\item Like using git or a similar VCS, the process encourages some good habits
\item Defining tasks and completing them concisely is valuable
\item Focus on the actual requirements of the code
\item Test suite built as you go
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Why Bother? (2/3)}
\begin{itemize}
\item Scientific computing is \textbf{difficult} because things can (and do) go wrong at every level of a deep ``stack'': hardware, system software, library software, application software, scripting software, algorithmic knowledge, mathematical knowledge, physical knowledge, scientific/domain knowledge.
\item You often have to move to new hardware, which you may not have root access to
\item You often have to move to use new operating systmes, compilers, libraries
\item Other people are constantly changing things
\item Floating point math in parallel is rarely bitwise reproducible
\item You have to work with large data sets which may have errors
\item You have to perform long runs which makes debugging difficult
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Why Bother? (3/3)}
\begin{itemize}
\item Algorithms do not work unconditionally, and you may not fully understand them (and no one on Earth might, either)
\item Documentation may be sparse
\item Code is either open source code which may have a small volunteer team and be in constant development, or be commerical code which is closed source and thus must be ``trusted'' and can never be modified.
\item Algorithms involve many types of errors/approximations: truncation/discretization error, modeling error, convergence tolerarances, empirical fits and interpolations, hacks, etc.
\item Tests are the \textbf{only way to have any confidence in anything}
\item However, you will use your simple test cases over and over!
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{How to get your code to work? Try to be smart, but realize you cannot think of everything}
\begin{itemize}
\item Technique 1:
\begin{itemize}
\item There is no substitute for deep understanding
\item Know what it is you are aiming for, and what fundamental limits there are on it (mathematically, physically, computationally, scientifically, etc.)
\end{itemize}
\item Technique 2:
\begin{itemize}
\item Recognize that you will never have deep enough understanding
\item Rely on hard evidence that things work
\item Separate your code's functionality into two categories
\begin{enumerate}
\item What it should do, based on your understanding of what you are doing
\item What you can prove it does with tests
\end{enumerate}
\item Offload sanity checks to the computer. It is not creative, but it doesn't get tired or distracted.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{The benefits of simple working code}
\begin{itemize}
\item Allows you to share code with other people, in other locations
\item Gives base code for experiments and extensions
\item Useful for porting to new environments
\item ``In telling someone else, you tell yourself''
\item Testing and prototyping can benefit each other. Your prototype will usually run quickly enough in your efficient implementation to be a test.
\item This is related to the benefits of rewriting (next week).
\item Mathematical fact: If you have a working state and a broken state, you will only need about $\log_2(N)$ tests to find the problem, where $N$ is the number of intermediate states. Even if $N$ is huge, $\log_2(N)$ is manageable. (see \texttt{git bisect}).
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Achieving Balance}
\begin{itemize}
\item As with prototyping, \textbf{Don't let the perfect be the enemy of the good}.
\item Tests are supposed to save you time, and there is a balance between testing nothing (bad)
and spending your life writing and debugging test harnesses instead of doing science (also bad).
\item As with most code, try to write in a way that can be extended, and don't try to do everything at once.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Test-driven design (TDD)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Test-Driven Design (TDD)}
\begin{itemize}
\item As with most advice about programming, a way of thinking about things, as much as a set of rules to be followed
\item Key feature: \textbf{write tests before you write the code} to pass the tests.
\item The TDD Cycle:
\begin{enumerate}
\item Decide on some new functionality
\item Write a failing test of that functionality
\item Write code to minimally pass the test 
\item Apply other good programming procedures (refactoring, etc.)
\item Confirm that the test still passes
\end{enumerate}
\item Steps 2 and 3 may need to be iterated, as you find that your test is too easy to pass because it doesn't test everything it is supposed to.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Benefits of TDD}
\begin{itemize}
\item The obvious drawback of TDD is that it takes time and thought
\begin{itemize}
\item May not pay off at the early prototyping and experimentation phases in Computational Science.
\end{itemize}
\item A less obvious drawback is that as the codebase becomes large, the time required to run tests may scale unacceptably
\item There are many benefits, though:
\begin{itemize}
\item Keeps you on task, on small achievable steps.
\item You will have a hierarchy of tests automatically, as you add more complexity to your code. These can be used for debugging.
\item You will be a bit more honest, tweaking the code to pass the test, not tweaking the test later to fit the code
\item Starting from a failing state lets you confirm that the test has meaning, and also lets you test your error-catching process.
\item If you find yourself being able to cheat a test, this indicates you need another test. (for example, if I only test my sine function for multiples of pi, I could write easier-than-expected code)
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{More on TDD}
\begin{itemize}
\item Let meaningful tests fail! 
\item Avoid randomness in tests
\begin{itemize}
\item This may seem like a good idea, as over time you will get better coverage of different cases
\item However, it means that you cannot infer much from a given run, and will have a hard time debugging
\item Forces the tester to think more - bad
\item Prefer reproducible pseudo-randomness, for instance use the same random seed with a portable Pseudo-Random Number Generator (PRNG) \footnote{not as easy as it sounds. Different systems may have differente implementations of \texttt{rand()} in their standard libraries!}
\end{itemize}
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Designing good tests for numerical software}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\frametitle{Defining a good set of tests}
\begin{itemize}
\item Include some trivial smoke tests, sanity tests, or ``Hello, World!'' type tests
\item Include a range of tests of increasing complexity
\item If it can run quickly enough, include a test which covers the most complex thing your code can do
\item Make it clear what component each test addresses
\item Document how to run the tests
\item Make it obvious how one might add more tests
\item Make sure the the whole (default) set of tests runs in a matter of minutes at most, on a laptop
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Some special considerations for Numerical PDE (1/2) }
\begin{itemize}
\item Consider your norms. 
\item In particular, if you conclude that you are ``close enough'' to a solution, are you measuring the true error? A residual of some function? 
\item For example, for a poorly-conditioned operator $A$, $r = b-Ax$ being small does not imply that $A^{-1}b-x$ is small.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Some special considerations for Numerical PDE (2/2)}
\begin{itemize}
\item Include both 'structured(symmetric)' and 'unstructured(nonsymmetric)' tests:
\item Test symmetric and nonsymmetric domains, as well as simple and small-but-complicated domains
\item Test symmetric and nonsymmetric matrices, if appropriate.
\item Test real and complex cases, if appropriate.
\item Test with zero forcing and nonzero forcing. 
\item Test with homogenous and inhomogenous BCs.
\end{itemize}
These sort of tests will expose two types of problem. 
\begin{itemize}
\item Nothing works - get the symmetric case working first.
\item The nonsymmetric case doesn't work when the symmetric case does, implying you have somehow assuming symmetry.
\item The symmetric case doesn't work when the symmetric case does, implying that you are relying 
on some averaging property or seeing a bug triggered by symmetry.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{The method of manufactured solutions {MMS}}
\begin{itemize}
\item Suppose one has method to solve a continuous linear PDE
$$
F(x) = b
$$
\item $F$ might involve derivatives of the function $x$
\item For most $b$, apart from simple or symmetrical choices, we typically don't know the exact solution $x$ to the continuous problem
\item This poses a challenge when attempting to assess the quality of a numerical procedure to approximate solutions to the PDE.
\item However, if one can often select a sufficently-complex solution $x_\text{manufactured}$ which satisfies any relevant boundary conditions, then one can use this to define $b = F(x_\text{manufactured})$
\item Now one has a continuous system with a known, non-trivial solution.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Testing in Practice for Computational Science}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Practical Approaches to Testing}
\begin{itemize}
\item Testing, like most good software practices, requires thought and time
\item Having a simple, minimal way to write regression tests is the first thing to do
\item Use an easy-to-use and easy-to-understand procedure, and document it
\item Once your code is large, consider introducing unit tests and integration tests
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Simple Tests with make/bash}
\begin{itemize}
\item As an example, you can use bash in your makefile like this
\begin{lstlisting}[language=bash]
test : test1 test2
test1 : myapp
  ./myapp -arg1 -arg2 2>&1 > test1.tmp
  diff test1.tmp testref/test1.ref && \ 
  echo "Success" || echo "!!! FAILURE !!! output of test 1 does not match reference"
  rm -f test1.tmp
\end{lstlisting}
\item Notes on bash used here:
\begin{itemize}
\item The cryptic \lstinline{2>&1} means ``redirect stderr(2) to stdout(1)''.
\item \lstinline{x && y || z} is equivalent to $\left((x \text{and} y) \text{or} z)\right)$. ``and'' is evaluated left to right, so this means ``try x. If it succeeds (returns 0), do y. If not, do z''.
\item This requires you to regenerate the reference output whenever you change your code, but this is a small price to pay to be able to quickly run sanity checks as you work.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Simple Tests with C++}
\begin{itemize}
\item Exceptions are a natural way to write tests in C++ itself
\item For example
\begin{lstlisting}[language=C++,
basicstyle=\tiny\ttfamily]
...
class TestFailureException{
  public:
    TestFailureException(std::string& msg) : msg_(msg) {}
    std::str what() const {return msg_;}
  private:
    std::string msg_;
};
...
try{
  Result result = FunctionToTest();
  if (result != expected_result) throw TestFailureException("Test X failed!");
}catch(TestFailureException e){
  std::cout << "Test X failed : " << e.what() << std::endl;
}catch(std::exception& e){
  std::cout << "Test X threw exception " << e.what() << std::endl;
}
...
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Making Things Easy}
\begin{itemize}
\item Tests are only valuable if people run them
\item Do what you can to make things work ``out of the box''. People walk away from code that doesn't work immediately.
\item Tests need to run quickly. For numerical problems, this means they should be on small domains with small numbers of timesteps.
\item Do everything you can to achieve ``one click'' testing
\begin{itemize}
\item The ideal is for the user to be able to type only \texttt{make test} or something similar
\end{itemize}
\item Use common tools likely to be available in most places(Cmake, make, python, c/c++ itself, perl, bash, probably ruby, ...).
\item Do not assume that your user knows \textbf{anything}. If the test says anything other than ``pass'', expect them to be confused.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Example}
\begin{itemize}
\item
Some demo code I wrote with some simple regression tests, which saved me huge amounts of time:
{\small \texttt{https://bitbucket.org/psanan/polykrylovpetscexample}}
\item Why does it work well?
\begin{itemize}
\item One click : \texttt{make test}
\item Yes/no output
\item runs quickly (on my laptop)
\item It lets me send the code to my collaborators and be confident it is still working
\item The code is documented with instructions on how to run the tests
\end{itemize}
\item It is not in any way perfect. Coverage isn't complete, it only computes diffs, not all tests pass cleanly, etc.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{More Elaborate testing}
\begin{itemize}
\item There are many libraries, procedures, books, etc. on testing software. 
\item There are people whose job is to do nothing else. 
\item We will not pursue these further as they are aimed at larger projects than almost anything you will find in computaional science. 
\item However, the principles are the same: testing is the only way to prove that software (continues to ) work, and working with testing in mind can produce better code in the process.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Assignment}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\begin{itemize}
\item Your assignment this week will be to port some MATLAB code to C++, and write some tests in the process.
\item Tests should include smoke, unit, regression, integration, and numerical convergence tests (which need not all be distinct!).
\item You will be provided with some MATLAB source to solve a simple PDE, courtesy of Drossos Kourounis
\item \texttt{week10/Assignment10.pdf} will be posted later today or tomorrow.
\end{itemize}
\end{frame}
\end{document}
