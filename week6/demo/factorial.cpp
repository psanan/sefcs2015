#include<stdexcept>

#include "factorial.h"

namespace link_demo {
  int Factorial(int n)
  {
    if(n < 0){
      throw(std::runtime_error("factorial not defined for negative arguments"));
    }
    if(!n){
      return 1;
    }
    int r = n;
    for(int s = n-1; s>1; --s){
      r *= s;
    }
    return r;
  }
}
