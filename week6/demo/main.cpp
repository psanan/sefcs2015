#include<iostream>

#include "factorial.h"

#define N 5

/* There could easily be a function with this name defined in some library,
   so we ensure that we use the one we want */
using link_demo::Factorial;

int main(int argc, char *argv[])
{
  std::cout << "A Simple Test Program to Examine Linking" << std::endl;
  std::cout << "0! = " << Factorial(0) << std::endl;
  std::cout << "1! = " << Factorial(1) << std::endl;
  std::cout << N <<"! = " << Factorial(N) << std::endl;
  try {
    std::cout << "(-9)! = " << Factorial(-9) << std::endl;
  } catch (std::exception& e){
    std::cout << "An error occurred computing (-9)! : " << e.what() << std::endl;
  }

  return 0;
}
