\input{"../templates/Lecture.tex.inc"}

\title[CEFCS Week 6]{Software Engineering for Computational Science: Week 6} 
\subtitle{The STL, Libraries and Linking}
\date{October 21, 2015} 

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\titlepage 
\end{frame}

\begin{frame}
\tableofcontents 
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Programming: The Standard Template Library}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[<+->][fragile]
\frametitle{The Standard Template Library (STL)}
\begin{itemize}
\item Knowledge of the STL is assumed with knowledge of C++.
\item This library accounts for many of the immediate benefits of switching from C to C++.
\item Contains:
\begin{itemize}
\item Containers (Data Structures)
\item Algorithms
\item I/O
\item Multithreading (C++11)
\end{itemize}
\item It's highly optimized.
\item It can add complication when debugging (see examples in the second half of the lecture)
\item We will do a brief tour, but \texttt{cplusplus.com/reference} has nice descriptions of the entire library.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Templates}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\frametitle{Templates}
\begin{itemize}
\item The problem: Write efficient parameterized code without re-writing functions over and over again.
\begin{lstlisting}[language=C++]
class Element2D
{
  //...
  private:
    double[3] indices_;
};
\end{lstlisting}
\begin{lstlisting}[language=C++]
class Element3D
{
  //...
  private:
    double[4] indices_;
};
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{Templates}
\begin{itemize}
\item A solution: Write templates which are processed to generate code which is then compiled
\begin{lstlisting}[language=C++]
template <int D> 
class Element
{
  //...
  private:
    double[D+1] indices_;
}
\end{lstlisting}
\item Templates allow for parameterized classes and functions, with no runtime overhead. \lstinline{Element3D} becomes \lstinline{Element<3>} in our example.
\item We will go into how to define your own templated classes and functions later in the course.
\item Using C++ templates can be tricky, but fortunately most of this complication is hidden from you when using the STL.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Containers}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Data Structures}

\begin{frame}[fragile]
\frametitle{Containers and Data Structures}
The STL provides containers which allow for use of all the most important data structures.
\begin{columns}
\column{0.5\textwidth}
\begin{itemize}
\item Array
\item Linked List
\item Stack
\item Heap
\item Hash Table
\item ..
\end{itemize}
\column{0.5\textwidth}
\begin{itemize}
\item \lstinline{vector}
\item \lstinline{list}
\item \lstinline{stack}
\item \lstinline{queue}
\item \lstinline{priority_queue}
\item \lstinline{set}
\item ..
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{\texttt{std::vector}}
\begin{itemize}
\item A wrapper around an array
\item Takes care of memory management
\item Random-access
\item Worse than linked lists for adding and removing internal elements
\item Example
\begin{lstlisting}[language=C++]
std::vector<double> v;
v.reserve(1024); 
for(int i=0; i<777; ++i){
  v.push_back(0.1 * i);
}
v[4535] = 4.5; // triggers malloc
std::cout << "The 7th element is " << v[6] << std::endl;
std::cout << "The number of elements stored is " << v.size() << std::endl;
// memory is freed when v goes out of scope
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{\texttt{std::list}}
\begin{itemize}
\item A doubly-linked list (see \lstinline{std::forward_list} in C++11, though)
\item No cheap random access
\item Cheap insertion and deletion
\begin{lstlisting}[language=C++]
std::list<int> l;
for(int i=1; i<10; ++i) l.push_back(2*i);
std::list<int>::iterator p = l.begin();
++p;
l.insert(p,7); //l is now 2 7 4 6 8 10 12 14 16 18
\end{lstlisting}

\item Difficult to sort
\begin{lstlisting}[language=C++]

\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{\lstinline{std::queue}, \lstinline{std::priority_queue}, and related structures}
\begin{itemize}
\item \lstinline{std::queue} describes a FIFO (``First in First Out'') structure
\item \lstinline{std::stack} describes a ``First In Last Out'' structure.
\item \lstinline{std::dequeue} (``Double-ended Queue'') describes a queue which can be modified from both ends
\item \lstinline{std::priority_queue} describes a set of entries with attached priorities, and can efficiently operate on the highest-priority entry. Typically implemented as a \emph{heap}.
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{\texttt{std::set} and related structures}
\begin{itemize}
\item A \lstinline{std::set} is a structure which can quickly add and remove unique entries, and report if an element is present
\item A \lstinline{std::map} is similar, but attaches data to each entry
\item \lstinline{std::multimap} and \lstinline{std::multiset} are similar, but relax the uniqueness requirement
\item These are typically implemented with a hash table.
\begin{itemize}
\item This is an extremely important thing to know about in computer science
\item These provide (amortized, effective) constant-time array lookup, which is a non-trivial capability.
\end{itemize}
\end{itemize}
\end{frame}

\subsubsection{Iterators}
\begin{frame}[<+->][fragile]
\frametitle{Iterators}
The way that algorithms and containers relate to each other.
\begin{itemize}
\item Any object which acts like an iterator is an iterator.
\item Iterators generalize the notion of pointers into an array, supporting operations like 
\begin{itemize}
\item Being incremented \lstinline{++i}
\item Being dereferenced \lstinline{*i},\lstinline{i->data_}
\item Being compared \lstinline{i != v.end()}
\end{itemize}
\item Some have more capabilities
\begin{itemize}
\item Being decremented \lstinline{--i}
\item Being ordered \lstinline{i < j}
\item ...
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{STL Iterators}
\begin{itemize}
\item More specifically, the STL defines 5 types of iterators:\footnote{Stroustrup, ``The C++ Programming Language, Chapter 33}
\begin{itemize}
\item Input
\item Output
\item Forward
\item Bidirectional
\item Random-access
\end{itemize}
\item Iterators can also have traits, in particular const iterators which allow read-only access.
\begin{lstlisting}[language=C++]
for(std::vector<double>::const_iterator i=v.begin(); i!=v.end(); ++i){
  std::cout << *i << std::endl;
}
\end{lstlisting}

\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{STL Strings}
\begin{itemize}
\item A very convenient class is \lstinline{std::string}.
\item It behaves much like a \lstinline{std::vector} of characters, with some additional functions.
\begin{lstlisting}[language=C++]
std::string v("Hello, World!\n");
v2 = v.substr(8); // "World!\n";
\end{lstlisting}
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Algorithms}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[<+->][fragile]
\frametitle{Sorting}
\begin{itemize}
\item STL Algorithm interact with STL containers via iterators
\item Sorting is simple, and appropriate algorithms are chosen for you based on the container type
\item Example
\begin{lstlisting}[language=C++]
#include <algorithm>
#include <vector>
...
std::vector<int> v;
...
std::sort(v.begin(),v.end());
\end{lstlisting}
\item Can define your own comparison functions.
\item \lstinline{std::stable_sort} to maintain order of equal elements.
\item \lstinline{std::partial_sort} to sort part of an array.
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{Searching}
\begin{itemize}
\item Return an iterator to an entry matching a value
\begin{lstlisting}[language=C++]
#include <algorithm>
...
std::vector<double>::iterator i;
i = std::find(v.begin(),v.end(),3.0);
if(i == v.end()){
  std::cout << "Element not found." << std::endl;
}
\end{lstlisting}
\item See also \lstinline{std::max_element} and others.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Others}
\begin{itemize}
\item \lstinline{<algorithm>} also includes many other useful tools
\begin{itemize}
\item Count entries matching a criterion
\item Find unique elements
\item Reverse
\item Maximum and minimum element (or other order statistics)
\item Remove elements matching a criterion
\end{itemize}
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{STL Input/Output}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[<+->][fragile]
\frametitle{STL Input/Output}
\begin{itemize}
\item We have seen examples of STL streams already
\begin{lstlisting}[language=C++]
std::cout << "Hello, World!" << std::endl;
std::cin >> x;
\end{lstlisting}
\item The STL also includes similar tools for working with strings and files
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{STL Input/Output}
\begin{itemize}
\item Strings
\begin{lstlisting}[language=C++]
#include<sstream>
...
std::stringstream ss;
ss << "Hello, World!" << std::endl;
std::cout << ss;
\end{lstlisting}
\item A \lstinline{std::string} can be referenced with the \lstinline{.str()} method.
\item Files
\begin{lstlisting}[language=C++]
#include <fstream>
...
std::fstream fs;
fs.open("file.txt",std::fstream::out);
fs << "Hello, World!" << std::endl;
fs.close();
\end{lstlisting}
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Break}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Tools: Linkers and Libraries}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Compilation Phases}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\frametitle{Compilation Phases}
\begin{itemize}
\item Preprocessing
\item Compilation
\item Assembly
\item Linking
\end{itemize}
\uncover<2->{
Why care? When things go wrong, it can help to be able to examine the intermediate stages
\begin{itemize}
\item \lstinline{gcc -E} [preprocess only]
\item \lstinline{gcc -S} [compile, don't assemble]
\item \lstinline{gcc -c} [assemble, don't link - we have seen this one already]
\end{itemize}
}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{The C Preprocessor}
\begin{itemize}
\item Often called the ``CPP'' (don't confuse this with the common \texttt{.cpp} extension for C++ source)
\item Processes a source file into a translation unit
\begin{itemize}
\item Removes comments
\item Does some potentially complex things to resolve all CPP macros (\lstinline{#include}, \lstinline{#define}, etc)
\begin{itemize}
\item Note that these are resolved before compilation, and obey different semantics than C/C++. Thus, if you are going to use CPP Macros, make sure it is obvious from your style choices that they are macros.
\end{itemize}
\item May do some manipulation of whitespace
\item Inserts markers, on lines beginning with \lstinline{#}
\item These tell you \lstinline{# line_number file_name flags}.
\item Flags: 
\begin{itemize}
\item 1- start of a new file
\item 2 - return to a file
\item 3 - text from a system header (suppress some warnings)
\item 4 - implicit \lstinline{extern "C"} 
\end{itemize}
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Exercise}
\begin{itemize}
\item In \texttt{week6/demo/}, try 
\begin{lstlisting}[language=C++]
gcc -E simple.c
\end{lstlisting}
\item Next, try
\begin{lstlisting}[language=C++]
g++ -E main.cpp > main.cpp.preprocessed
\end{lstlisting}
and examine the output.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Preprocessor: warnings}
\begin{itemize}
\item The preprocessor should be used with care
\begin{itemize}
\item It does not obey the semantics of C or C++
\item It can create confusing bugs, especially when using external libraries
\end{itemize}
\item Prefer not using it, in general.
\item Tips:
\begin{itemize}
\item If you \lstinline{#define} something in a file and only use it in that file, \lstinline{#undef} it at then end of the file
\item Otherwise, consider \emph{namespacing}.
\begin{lstlisting}[language=C++]
/* Bad */
#define ITERATIONS 10 
/* Slightly less bad */
#define MY_PACKAGE_NAME_ITERATIONS 10
\end{lstlisting}
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{The Compiler}
\begin{itemize}
\item Responsible for taking the output of the preprocessor and producing assembler language.
\item This is processor-specific and difficult to read.
\item It corresponds to the low level instructions that the processor will use to execute your program.
\item You usually don't need to worry about this, but if you are wondering what your optimizing compiler did, this can be a way to see.
\item Exercise: in the \texttt{week6/demo/} subdirectory of the course repository, try
\begin{lstlisting}[language=C++]
gcc -S simple.c
less simple.s
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{The Assembler}
\begin{itemize}
\item The assembler translates assembly language into executable code.
\item Exercise
\begin{lstlisting}[language=C++]
gcc simple.c
hexdump a.out
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{The Linker}
\begin{itemize}
\item After compilation, there might still be function calls where the location of the code to execute is unknown
\item The linker combines several pieced of code and resolves these function calls to actual memory locations
\item The result is a piece of executable code.
\item We will see that there are a couple of variants on this process.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Exercise}
\begin{itemize}
\item In \texttt{week6/demo/}, look at the makefile to determine how \lstinline{main} is built
\item Ignore \lstinline{main2} and \texttt{main3} for now
\item Examine the preprocessor output, object files, and assembler code for the intermediates.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Linking}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[<+->][fragile]
\frametitle{The Symbol Table}
%relation to debugging symbols
\begin{itemize}
\item We need a way to associate names with locations in memory.
\item Objects and libraries contain data structures to do this
\item You can examine these with the \texttt{nm} tool.
\begin{itemize}
\item Gives a value for each symbol (a memory location)
\item Specifies the type of each symbol.
\item Pay particular attention to the \texttt{U} entries, which are undefined and must be resolved by the linker
\item Consider using \lstinline{nm -C} which will ``demangle'' some C++ symbols.
\end{itemize}
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Static Libraries}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[<+->][fragile]
\frametitle{Static Libraries}
\begin{itemize}
\item Similar to an object file.
\item Can be thought of simply as a collection of object files
\item Typically has extension \texttt{.a}
\item Has a single symbol table
\item Link order within the library is not an issue.
\item Unlike object files, the compiler will only look for symbols in library files if any are unresolved.
\item Only required objects will be included.
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{Creating Static Libraries}
\begin{itemize}
\item A static library is an \emph{archive} plus an \emph{index}
\item An archive is a collection of files grouped together.
\item Create an archive with a symbol table with
\begin{lstlisting}[language=C++]
ar -s libmine.a obj1.o obj2.o
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{Using Static Libraries}
\begin{itemize}
\item Several options to link
\begin{itemize}
\item As you would an object file
\begin{lstlisting}[language=C++]
gcc -Wall main.o libmine.a -o runMe
\end{lstlisting}
\item using shorthand
\begin{lstlisting}[language=C++]
gcc -Wall -lmine -L. -o runMe
\end{lstlisting}
\end{itemize}
\item You can tell gcc where to look for libraries with the \lstinline{-L} flag.
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{Linking Order}
\begin{itemize}
\item GCC links libraries \textbf{left to right}
\item Recall that objects are always included, so link order doesn't matter there.
\item This means that it keeps track of which symbols are unresolved.
\item This can be annoying but, when working with large libraries, it drastically speeds up linking time
\item Exercise. In \texttt{week6/demo}, fix this:
\begin{lstlisting}[language=bash]
make
g++ -Wall libfactorial.a main.o -o main2
\end{lstlisting}
Why did your fix work?
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Exercise}
\begin{itemize}
\item Examine how \texttt{week6/demo/main2} is built
\item Use \lstinline{nm} to compare the symbol tables for \texttt{main} and \texttt{main2}.
\item What is the difference? %one includes the UselessFunction
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dynamic and Shared Libraries}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\frametitle{Dynamic Linking}
\begin{itemize}
\item Motivation: it is wasteful to include the same code, especially system code, in every executable
\item Motivation: it is wasteful to load the same code multiple times for different applications
\item Motivation: it is wasteful to load code that will never be used
\item Dynamic linking and Dynamic loading delay parts of the linking process to try and address these problems
\item Often efficient, but more complex.
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{Loading}
\begin{itemize}
\item Linking can be completed at several points
\item When compiling the executable (static linking)
\item When loading the program into main memory before it is run (dynamic linking)
\item After the program has been loaded, as it is running (dynamic loading)
\end{itemize}
\end{frame}

% Compiling a shared library
\begin{frame}[<+->][fragile]
\frametitle{Compiling and linking witha shared Library}
\begin{itemize}
\item Varies with compilers
\item GCC compilation
\begin{itemize}
\item Supply the \lstinline{-shared} flag
\item Supply the \lstinline{-fPIC} flag for all objects being included
\end{itemize}
\item To use the library, proceed as with a static library
\begin{lstlisting}[language=C++]
g++ -Wall main.o -lmine -o main
\end{lstlisting}
\item To specify where to look for shared libraries, pass flags to the linker
\begin{lstlisting}[language=C++]
g++ -Wall -Wl,-rpath,/path/to/libmine.so main.o -lmine -o main
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Exercise}
\begin{itemize}
\item Return to \texttt{week6/demo/}
\item Build \texttt{main3} and examine the process
\item This might require looking up some variants on the flags used (which are for GCC on a linux machine)
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{Where are shared libraries found?}
\begin{itemize}
\item Unix-based systems tend to use similar names for shared libraries. The details are not so important here, but to try and de-mystify a bit, they are
\begin{itemize}
\item The ``soname'', like \lstinline{libmine.so.3}
\item The ``real name'', like \lstinline{libmine.so.3.4.8}
\item Another name which is used by the linker, like \lstinline{libmine.so}
\end{itemize}
\item This is all to allow these libraries to be updated continuously. The major release number corresponds to a stable API.
\item Use \lstinline{ls -l} to see where these links point.
\item the linker (usually \lstinline{ld}) has its own rules of where to look for these libaries.
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{Dynamic Loading}
\begin{itemize}
\item Delay resolutions of symbols even longer, to run time.
\item This allows plugin-type models
\item Don't load code unless you are going to use it!
\item We won't cover this, but if you see errors around \lstinline{dlopen} at somepoint in your career, this is the underlying process.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Debugging Linking Issues}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{The Golden Rule}
\begin{itemize}
\item Read the error messages.
\item If a symbol can't be found, why not?
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{NM}
\begin{itemize}
\item If a symbol is undefined, use nm
\item Is the symbol defined in the file you thought it was?
\item Is the link order correct?
\item Is the correct file being linked?
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{LDD}
\begin{itemize}
\item With shared libraries, one cannot know until loadtime if the program will run
\item \lstinline{ldd} tells you which shared libraries will be examined to resovle undefined symbols
\begin{lstlisting}[language=bash]
$ ldd /bin/cat
linux-vdso.so.1 =>  (0x00007fff2e191000)
libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f3c1435b000)
/lib64/ld-linux-x86-64.so.2 (0x00007f3c14720000)
\end{lstlisting}
\item on OS X, there is a similar thing called otool
\begin{lstlisting}[language=C++]
otool -L /bin/ls
\end{lstlisting}
\item Exercise: Examine \texttt{week6/demo/main3} with one of these tools, and look to see where the actual libraries are with \lstinline{ls -l}
\end{itemize}
\end{frame}

\begin{frame}[<+->][fragile]
\frametitle{Common Linker Hacks}
\begin{itemize}
\item The environment variable \lstinline{LD_LIBRARY_PATH} is often used to let the linker find a missing shared library
\item However, this is global and fragile
\item Prefere to specify the path when you compile (see \texttt{week6/demo/makefile})
\item \lstinline{LD_PRELOAD} can be set to specify which locations to look in first
\item Again, a bit of a hack, and there are better controls available with linker flags
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Assignment}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Assignment}
\begin{itemize}
\item Revisit the Lorenz99 System, but this time use the STL and the Eigen library (for implicit time stepping) to do a better job.
\item \texttt{Assignment6.pdf} will be posted later today.
\end{itemize}
\end{frame}
\end{document}
