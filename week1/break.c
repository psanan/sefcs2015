#include<stdio.h>
#include<unistd.h>

int main()
{
  const int kBreakMinutes = 10;
  const int kSecondsPerMinute = 60;

  printf("Please enjoy your %d minute break\n", kBreakMinutes);
  for(int i=kBreakMinutes; i>0; --i){
    printf("  You have %d minutes remaining\n", i);
    sleep(kSecondsPerMinute);
  }
  printf("It's time to start again!\n");
  return 0;
}
