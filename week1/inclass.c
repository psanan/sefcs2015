/*
  Solution for in-class exercise.

  Prints the numbers from 10 to 1, except 7, to the standard output,
  followed by a newline.

  To compile, use e.g.
    gcc -Wall -std=c99 -o inclass inclass.c

  To run, use 
    ./inclass
*/
#include<stdio.h>

int main()
{
  const int kMaxIter = 10;
  const int kMinIter  = 1;
  const int skip      = 7;
  for(int i=kMaxIter; i>=kMinIter; --i){
    if(i != skip){
      printf("%d ",i);
    }
  }
  printf("\n");
  return 0;
}
