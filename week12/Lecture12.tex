\input{"../templates/Lecture.tex.inc"}

\title[CEFCS Week 12 ]{Software Engineering for Computational Science: Week 12} 
\subtitle{Build Systems and Rewriting/Refactoring}
\date{December 2, 2015} 

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\titlepage 
\end{frame}

\begin{frame}
\tableofcontents 
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Build Systems}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Build Systems: Motivation}
\begin{itemize}
\item Makefiles are a major step towards portable code
\item However, details of different systems usually prevent a user from building and running your code with no intervention at all.
\item For small or simple projects, one can provide the user with simple-enough instructions
\begin{lstlisting}[language=C++]
# Define this variable to build on OS X
OSX=1
ifdef OSX
OPENGL_LIB=-framework OpenGL -framework GLUT
else
OPENGL_LIB=-lGL -lGLU -lglut
endif
\end{lstlisting}
\item For larger projects and those which build libraries to be installed in standard locations, this becomes cumbersome.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Build Systems: Motivation, cont.}
\begin{itemize}
\item For more complex projects, expecting the user to spend time tweaking makefiles can drive people away
\item A \emph{build system} attempts to provide a platform-independent build and install process
\item In some cases, this can provide truly braindead compilation like
\begin{lstlisting}[language=C++]
git clone git@bitbucket.org:myhandle/myproject
cd myproject
mkdir build
cd build
cmake ..
make
make test
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Build Systems: Motivation, cont.}
\begin{itemize}
\item The most common tools
\begin{itemize}
\item GNU Autotools (what you are usually using if you run \lstinline{./configure && make && make install})
\item CMake (what we will use as an example today)
\item SCONS, Rake, etc.
\item Custom build systems
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[t,fragile]
\frametitle{Build Systems : Pros and Cons}
\begin{columns}
\column{0.5\textwidth}
\textbf{Pro}
\begin{itemize}
\item Portability
\item Careful consideration of dependencies
\item Ease of testing on multiple systems
\end{itemize}
\column{0.5\textwidth}
\textbf{Con}
\begin{itemize}
\item Developer overhead
\item Overkill for small projects
\item High-level, confusing when it breaks
\item Both CMake and Autotools are eventually hated by everyone who uses them. They can be friendly for users, but rarely are for developers.
\end{itemize}
\end{columns}
\vspace{10pt}
Should you use them? Yes, if you have no choice, or if the testing framework allows you to be productive.
\end{frame}

\begin{frame}[fragile]
\frametitle{Out-of-source Builds}
\begin{itemize}
\item As the name implies, build your code in a different directory than your source code
\item Many benefits
\begin{itemize}
\item Do not clutter your source tree
\item Easy to clean
\item Easy to build for multiple architectures
\end{itemize}
\item Encouraged/enforced by many build systems, including CMake
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{CMake}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{CMake}
\begin{itemize}
\item A commonly-used build system
\item You will likely have to configure packages using it at some point
\item Like make, centered around a file defining a set of ruls and procedures to build one or more pieces of executable code
\item Includes additional capabilities, including for testing and installation
\item Generates native build tools (by default, makefiles)
\item Works well with C++ by default \footnote{For C/Fortran, remember \lstinline{ENABLE_LANGUAGE(C)} or \lstinline{ENABLE_LANGUAGE(Fortran)}}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{CMake: basic usage}
\begin{itemize}
\item
For simple projects, CMake is used much like make
\begin{lstlisting}[language=C++]
# define an executable target
add_executable(runMe main.cpp)

# Specify locations for headers
include_directories(${PROJECT_SOURCE_DIR}/h)

# Link libraries
target_link_libraries(runMe ${PNG_LIBRARY})
\end{lstlisting}
\item 
The \lstinline{cmake} programe converts a text file \texttt{CMakeLists.txt} into a makefile or configuration for another build tool
\item 
This makefile defines commont targets
\begin{lstlisting}[language=C++]
make
make clean
make install
\end{lstlisting}
\item Each directory in your source tree (referred to with \lstinline{add_subdirectory})  must include \texttt{CMakeLists.txt}.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Ways to run CMake}
There is no "cmake clean", so out-of-source builds are the typical practice
\begin{itemize}
\item From the command line
\begin{lstlisting}[language=C++]
mkdir build
cd build
cmake ..
\end{lstlisting}
\item Using \lstinline{ccmake}
\begin{lstlisting}[language=C++]
mkdir build
cd build
ccmake ..
\end{lstlisting}
\item With a gui tool (a prettier version of \lstinline{ccmake})
\end{itemize}
This generates 
\begin{itemize}
\item \lstinline{CMakeCache.txt} which can be though of as a configuration file. You can edit this.
\item and auxiliary files in \lstinline{CMakeFiles}
\item (by default) a makefile
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Useful CMake global variables}
\begin{itemize}
\item \lstinline{CMAKE_BINARY_DIR} :
The top level directory of the build tree.
\item
\lstinline{CMAKE_SOURCE_DIR} :
The top level source directory
\item
\lstinline{EXECUTABLE_OUTPUT_PATH} :
destination for all executable files (default is \lstinline{CMAKE_CURRENT_BINARY_DIR})
\item
\lstinline{LIBRARY_OUTPUT_PATH}
destination for all libraries
\item \lstinline{PROJECT_NAME}
set by \lstinline{project()}
\item 
\lstinline{PROJECT_SOURCE_DIR}
path to the root of your project source directory (where CMakeLists.txt calls \lstinline{project()}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{The Generated Makefile}
\begin{itemize}
\item Simple enough to read
\item Supports \lstinline{make help}
\item To see the commands as they are executed
\begin{lstlisting}[language=C++]
make VERBOSE=1
\end{lstlisting}
\item Note \lstinline{make edit_cache}, which launches \lstinline{ccmake} to allow you to reconfigure.
\end{itemize}
\end{frame}


\begin{frame}[fragile]
\frametitle{Parallels with Make}
\begin{itemize}
\item Targets: Like make, usually executables or libraries. ``Phony'' targets can also be introduced
\begin{lstlisting}[language=C++]
add_custom_targe(run ./main)
\end{lstlisting}
\item Comments: CMake input files can (and should) contain comments, like makefiles
\item Variables: CMake also maintains its own set of variables, like Make, referenced in the same way,
\begin{lstlisting}[language=C++]
${VAR}
\end{lstlisting}
\item Access invironment variables with 
\begin{lstlisting}[language=C++]
$ENV{VariableName}
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Creating Libraries}
\begin{itemize}
\item Libraries are much like other targets, but accept some additional arguments
\begin{lstlisting}[language=C++]
add_library(mylib STATIC ${SOURCES})
install(TARGETS mylib DESTINATION /usr/lib)
\end{lstlisting}
\item Shared libraries can also be created
\begin{lstlisting}[language=C++]
add_library(mylib SHARED ${SOURCES})
install(TARGETS mylib DESTINATION /usr/lib)
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Testing with CMake}
\begin{itemize}
\item CMake ships with CTest, which lets you include tests in your build information
\item These don't do much on their own, but importantly they allow the user to run \lstinline{make test} easily, which is a big benefit
\item In the simplest form, just provide executables which return 0 for success and non-zero for failure.
\item Minimal syntax
\begin{lstlisting}[language=C++]
add_executable(testexecutable testexecutable.cpp)
enable_testing()
add_test(mytest testexecutable)
\end{lstlisting}
\item By default, run with \lstinline{make test}
\item You can also check that the output of a test matches a particular regular expression
\begin{lstlisting}[language=C++]
add_test(outtest outtest)
test_properties(outtest PROPERTIES PASS_REGULAR_EXPRESSION Keyphrase)
\end{lstlisting}
\end{itemize}
\end{frame}


\begin{frame}[fragile]
\frametitle{Finding and using libraries}
\begin{itemize}
\item \lstinline{find_package(XYZ)} makes it simple provided there is an available \emph{module} (named \lstinline{FindXYZ}).
\item Many commonly-used libraries are supported (but not all)
\begin{lstlisting}[language=C++]
find_package(PNG REQUIRED)
include_directories(${PNG_INCLUDE_DIR})
target_link_libraries(main ${PNG_LIBRARY})
\end{lstlisting}
Modules define variables you can use 
\begin{lstlisting}[language=C++]
PNG_INCLUDE_DIRS, where to find png.h, etc.
PNG_LIBRARIES, the libraries to link against to use PNG.
PNG_DEFINITIONS - You should add_definitons(${PNG_DEFINITIONS}) before compiling code that includes png library files.
PNG_FOUND, If false, do not try to use PNG.
PNG_VERSION_STRING - the version of the PNG library found (since CMake 2.8.8)
\end{lstlisting}\texttt{\tiny https://cmake.org/cmake/help/v3.0/module/FindPNG.html}
\item You can also download modules for other libaries and let CMake know where to look for them (See \texttt{https://cmake.org/Wiki/CMake:How\_To\_Find\_Libraries}).
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{CMake Policies}
\begin{itemize}
\item Because the entire point of CMake is to allow stable, automatic builds, fixing bugs in CMake itself is problematic. Breaking backwards-compatibility is a hard decision for them to make. 
\item Policies document every backwards-incompatible change made in CMake. Each has a NEW and OLD behavior. The NEW behavior is preferred over OLD, but breaks some existing builds.
\item This is why CMake nags you to include the version. This is the simplest way to manage policies - just choose NEW for every policy up to the minimum version, and OLD after that.
\item If you depend on OLD when you shouldn't, code breaks.
\item You can also set each policy individually \lstinline{cmake_policy(SET CMP<NNNN> NEW)} or \lstinline{cmake_policy(SET CMP<NNNN> OLD)}
\item \texttt{https://cmake.org/Wiki/CMake\_Policies}
\end{itemize}
\end{frame}


\begin{frame}[fragile]
\frametitle{Version Numbers}
\begin{itemize}
\item Given the importance of versioning, by the time you are using a build system you will certainly want to specify a version number
\item A standard way to do this in CMake is to process some values
\begin{lstlisting}
project(demo)
set(demo_VERSION_MAJOR 0)
set(demo_VERSION_MINOR 1)
\end{lstlisting}
\item CMake also includes its own preprocessing which can do things like insert these values into your code.
\item \lstinline{configure_file()} processes values surrounded by \lstinline{@} to be equal to their values as CMake variable.
\begin{lstlisting}[language=C++]
configure_file(
  "${PROJECT_SOURCE_DIR}/version.h.in"
  "${PROJECT_BINARY_DIR}/version.h"
  )
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{A Simple Demo}
\begin{itemize}
\item The most basic usage to build an executable \lstinline{main} out of \lstinline{main.cpp} is to define \lstinline{CMakeLists.txt} with
\begin{lstlisting}[language=C++]
add_executable(main main.cpp}
\end{lstlisting}
and then execute \lstinline{cmake; make}
\item We will show a slightly more complicated demo, including some of the topics we have covered.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{More resources}
\begin{itemize}
\item \lstinline{cmake --help} 
\item \lstinline{cmake help --help-manual cmake-buildsystem }
\item \texttt{https://cmake.org/documentation/}
\item \texttt{https://cmake.org/cmake-tutorial/}
\item Examine \lstinline{CMakeLists.txt} from libraries you are familiar with
\texttt{https://en.wikipedia.org/wiki/CMake\#Notable\_applications\_that\_use\_CMake}
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Refactoring and Rewriting}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\frametitle{Rewriting}
\begin{itemize}
\item "Writing is rewriting" - Hemingway
\item There is always some sort of iterative process with software development - it's impossible to avoid making decisions which need to be reversed.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Rewriting Numerical Code}
\begin{itemize}
\item Writing a numerical kernel several times helps you to understand the algorithm
\item Mathematical bugs can be caught this way
\item There is usually an efficiency gain
\item With proper unit tests, the rewrite can double as an optimization pass
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{When not to rewrite?}
\begin{itemize}
\item Rewriting takes time
\item Existing code has been tested and debugged
\item It's easier to write code than read it: do you know what it is you're rewriting? Are you rewriting because you are too lazy to read and/or refactor?
\item Generally, the more well-tested and non-performance-critical a piece of code is, the harder you should think before rewriting it.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Refactoring}
\begin{itemize}
\item A commonly-used term in software engineering.
\item Restructuring and reorganization of code, leaving the functionality invariant
\item Commonly done in C++ when clean coding practices are violated, or when class hierarchies need to be reconsidered
\item Almost always helps the developer understand the logical abstractions being used in a piece of code better
\item You should do it when you can ``smell''\footnote{actually a fairly standard term} that failing to do it will cost you time.
\item Classic example : code duplication
\item Classic example : bad naming. Do you always guess the wrong name for something? Why?
\item Classic numerical example: replace a working algorithm or routine with a better one, in the sense of resource usage (flops, memory, communication,..), 
numerical stability, clarity, (Generality (does it work in complex arithmetic?), etc.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Refactoring to avoid code duplication}
\begin{itemize}
\item Code duplication is usually a bad thing
\begin{itemize}
\item More code to read, maintain, debug
\item Changes must be synchronized, which is practically impossible
\item A missed opportunity for encapsulation
\end{itemize}
\item Sometimes code is duplicated for efficiency reasons (say when the alternative is some overly-clever preprocessing, or reliance on a particular compiler optimization)
\item For example, if you find yourself writing the same operation (say in inner product) over and over, consider how you might encapsulate the operation in a function [or better yet for this example, leverage a library].
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Assignment}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Assignment}
Use a build system to allow your port from last week to be built and tested on multiple systems (OS X and Ubuntu will be tested). See \texttt{week12/Assignment12.pdf}
\end{frame}
\end{document}
