template<typename S>
void process(S s){
  //typename S::datatype * var; //works!
  S::datatype * var; // fails!

  *var = 3 * S::dimension;
}

class Problem
{
  public: 
    typedef int datatype;
    const static int dimension = 2;
};

int main()
{
  Problem problem;
  process(problem);
}
