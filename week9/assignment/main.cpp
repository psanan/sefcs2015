#include <iostream>
#include "mesh.h"

using std::cout;
using std::endl;

int main(int argc, char* argv[])
{
  Mesh mesh;

  try{
    mesh.AddTriangle(0.0,1.0,1.0,0.0,1.0,1.0);
  }catch (std::exception& e){
    cout << "Exception caught trying to add triangle 2: " << e.what() << endl;
  }

  try{
    double area = mesh.Area();
    cout << "With one triangle added, the area is " << area << endl;
  }catch (std::exception& e){
    cout << "Exception caught computing the area: " << e.what() << endl;
  }

  try{
    mesh.AddTriangle(1.0,1.0,2.0,1.0,2.0,1.5);
  }catch (std::exception& e){
    cout << "Exception caught trying to add triangle 2: " << e.what() << endl;
  }

  try{
    mesh.AddTriangle(1.0,0.0,2.0,1.0,1.0,1.0);
  }catch (std::exception& e){
    cout << "Exception caught trying to add triangle 3: " << e.what() << endl;
  }

  try{
    double area = mesh.Area();
    cout << "With three triangles added, the area is " << area << endl;
  }catch (std::exception& e){
    cout << "Exception caught computing the area: " << e.what() << endl;
  }

  try{
    double area = mesh.Area();
    cout << "With three triangles added, the area is still " << area << endl;
  }catch (std::exception& e){
    cout << "Exception caught computing the area: " << e.what() << endl;
  }
  
  try{
    mesh.AddTriangle(1.0,0.0,0.0,1.0,1.0,1.0);
  }catch (std::exception& e){
    cout << "Exception caught trying to add Triangle 4: " << e.what() << endl;
  }

  try{
    double area = mesh.Area();
    cout << "With four triangles added, the area is " << area << endl;
  }catch (std::exception& e){
    cout << "Exception caught computing the area: " << e.what() << endl;
  }

  return 0;
}
