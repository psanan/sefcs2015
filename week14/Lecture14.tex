\input{"../templates/Lecture.tex.inc"}

\title[CEFCS Week 14 ]{Software Engineering for Computational Science: Week 14} 
\subtitle{Advanced C++ and Final Preliminary Discussions}
\date{December 16, 2015} 

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\titlepage 
\end{frame}

\begin{frame}
\tableofcontents 
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Advanced C++}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{C++11 Threads}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\frametitle{Shared-memory Parallelism}
\begin{itemize}
\item A shared-memory parallel environment is one in which several processes run while having access to the same memory space.
\item This is very useful, as it allows independent parts or your program to progress as separate processes. They don't block each other, and especially on modern hardware, they can procees simultaneously.
\item However, this adds an element of nondeterminism to your code, greatly increasing the potential for bugs.
\item Many times, these independent processes are called \emph{threads}
\item \emph{thread safety} is a property of code. It means that code will behave correctly no matter how quickly or slowly each thread performs its individual tasks
\item Ensuring thread safety mainly involves guarding regions of memory which may be accessed by multiple threads, and avoiding \emph{race conditions}, where the correctness of the code depends on the relative speed of the threads.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Atomic Operations}
\begin{itemize}
\item For most non-trivial multithreaded applications, some operations must be known to be \emph{atomic}, meaning that they either do not occur, or occur completely
\begin{itemize}
\item Setting a byte in memory is atomic. 
\begin{lstlisting}[language=C++]
bool b;
b = true;
\end{lstlisting}
\item Incrementing an integer is not atomic
\begin{lstlisting}[language=C++]
int x = 1;
x++;
\end{lstlisting}
because it actually involves several atomic operations
\end{itemize}
\item Threading libraries provide ways to make more complex operations atomic, at some cost.
\item There is a notion of atomicity which also requires that all processes agree on the value of some memory. This should not be assumed unless explicitly guaranteed.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Locks}
\begin{itemize}
\item A fundamental object is an atomic lock
\item Also called a \emph{mutex} (for something \textbf{mut}ually \textbf{ex}clusive\footnote{The plural of ``mutex'' is ``mutexes'', not ``mutices'' (the word is a concatenation of two abbreviated English words, not a word of Latin origin.)})
\item This is something which can be locked by some thread and only locked by another thread after the first thread unlocks it.
\item This property allows any other operation to be protected.
\item Aside : you may see some discussion of \lstinline{volatile} in tutorials on threading, but it's best to avoid using this unless you are doing something quite advanced.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{C++11 Threads}
\begin{itemize}
\item Before C++11, the C++ language and library did not address shared memory parallelism, hence there was no standardized way to abstract it.
\item C++11 introduces a native implementation of threads in the standard library
\begin{lstlisting}[language=C++]
#include <thread>
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Forking and Joining threads}
\begin{itemize}
\item The program's \lstinline{main} function runs on one thread, which typically creates more.
\item Threads are typically \emph{forked} (one process becomes two) and \emph{joined} (two processes become one).
\item In C++11, one forks a thread simply by creating a \lstinline{std::thread} object
\begin{lstlisting}[language=C++]
void myFunction(int arg1, double arg2);
//..
std::thread mythread(myFunction,arg1,arg2);
\end{lstlisting}
\item Threads are joined simply with
\begin{lstlisting}[language=C++]
mythread.join();
\end{lstlisting}
which will block until the thread completes its task (the function returns).
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Using Mutexes}
\begin{itemize}
\item Mutexes are very simple to use with C++11 (and indeed with \lstinline{pthreads} and any other threading library)
\item Example
\begin{lstlisting}[language=C++]
class MyDataClass{
  public:
    void update(){
      data_lock_.lock();
      data_.update();
      data_lock_.unlock();
    }
  private:
    mutable std::mutex data_lock_;
    Data data_;
}
\end{lstlisting}
\item When in doubt, protect with a mutex or protect with \lstinline{std::atomic} (next slide)
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Using Atomic Types}
\begin{itemize}
\item The C++11 standard library introduces atomic types
\item These are guaranteed to be thread safe
\item They \emph{may} be ``lock free'' (that is, they are guaranteed never to let one thread's lack of progress halt another thread indefinitely). Check with \lstinline{std::atomic<T>::is_lock_free}
\item These objects are immune to data races. That is, if two threads operate on the same data at the same time, the results will of course depend on which thread ``gets there first'', but the threads will always agree on the value of the variable.
\item Perfect for counters which need to incremented/decremented.
\begin{lstlisting}[language=C++]
std::atomic<int> counter;
\end{lstlisting}
\item You might be tempted to do something like \lstinline{std::atomic<std::vector<double>>}, but the implementation requires copying, so you are better off with mutexes.
\end{itemize}
\end{frame}

% Skipped
%\begin{frame}[fragile]
%\frametitle{Condition Variables}
%% Why? Easier and perhaps more efficient way to wait for some condition to be true
%% You can implement your own with the other tools, but this is convenient and lets you pass some work off to the implementers of the std library.
%% http://stackoverflow.com/questions/16350473/why-do-i-need-stdcondition-variable
%% http://codexpert.ro/blog/2013/03/01/cpp11-concurrency-condition-variables/
%\end{frame}

\begin{frame}[fragile]
\frametitle{Higher-level operations}
\begin{itemize}
\item The C++11 standard library provides some even higher-level tools
\item One handy tool which can use threads in the background is to declare an asynchronous task (akin to a non-blocking MPI operation).
\begin{lstlisting}[language=C++]
std::future<int> value = std::async(std::launch::async,FunctionToComputeValue);
\end{lstlisting}
\item The \lstinline{std::future<T>} template allows you to declare a memory location to be filled ``lazily'' at some point before its value is needed.
\item Related is \lstinline{std::promise<T>} which provides a place to asynchronously write a value
\begin{lstlisting}[language=C++]
std::promise<int> promise;
// pass promise to a thread which can write to it
std::future<int> promise.get_future(); // later retrieve the value as a future to read
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Debugging multithreaded code}
\begin{itemize}
\item Best practices for sequential code still apply (isolate and reproduce the problem, follow clean coding practices, etc.)
\item However, the major additional complication is that multithreaded code behaves \emph{nondeterministically}; you cannot rely on a 1-1 mapping between inputs and outputs anymore.
\item For this reason, your ability to reason becomes more important - you must be able to consider all possible cases.
\item Practical tip (without resorting to elaborate profiling environments): add overzealous locks and logging.
% There are probably more..
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Example}
See \lstinline{week14/example}. Note that to compile on some systems/compilers, you may need to include the \lstinline{-pthread} flag (as the pthread library can be used to implement \lstinline{std::thread}).
% ..
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Other C++11 Features}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Lvalues and Rvalues, and move semantics}
\begin{itemize}
\item An \emph{lvalue} ("locator" or "left hand side") is something which you can take the address of. Most variables fall into this category. 
\item An \emph{rvalue} is a temporary object. That is, something that is not being relied upon by the programmer to exist at a given address.
\item Consider this example
\begin{lstlisting}[language=C++]
std::string func(){
  return std::string("rhubarb");
}
std::string x = func();
\end{lstlisting}
This copies a string \textbf{twice}, first to a temporary object, and then to \lstinline{x}.
\item C++11 adds a new type of variable, an \emph{rvalue reference} like \lstinline{int&&}.
\item This allows you to overload functions to gain efficiency.
\item One thing that this allows is \emph{move semantics} in which resources can be ``stolen'' instead of copied because they are known to come from a temporary object.
\item In our example, we (and the C++ standard library) can define a new assignment operator
\begin{lstlisting}[language=C++]
std::string::operator=(std::string&& s);
\end{lstlisting}
This is free to steal the resources from \lstinline{s} instead of copying them.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Automatic Type Deduction}
\begin{itemize}
\item C++11 adds the \lstinline{auto} keyword.
\item This can be used when the compiler can unambiguously determine a type.
\item This makes for less-safe and less-clear code, often, but in some cases is handy
\begin{lstlisting}[language=C++]
std::vector<std::pair<int,float>> v;  // NOT a typo in C++11
//..
for(auto i=v.begin(); i!=v.end(); ++i){
  //...
}
\end{lstlisting}
\item Makes some new things possible with templates:
\begin{lstlisting}[language=C++]
template <typename T> void func(T x){
    auto y = x.someMethod(); // return type can depend on T
};
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{New \texttt{for} loop syntax}
\begin{itemize}
\item C++03 for loops can be verbose and potentially inefficient.
\item C++11 introduces a new syntax for for loops %http://en.cppreference.com/w/cpp/language/range-for
\begin{lstlisting}[language=C++]
std::vector<int> v;
for(const int& i : v){ // you can also use auto, auto& 
  std::cout << *i << " " << std::endl;
}
\end{lstlisting}
\item The example above depends on \lstinline{begin()} and \lstinline{end()} being properly defined (as they are for everything in the STL)
\item The standard library already included \lstinline{std::for_each}, which behaves similarly, and has been updated with proper move semantics in the C++11 standard library.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{\texttt{delete}}
\begin{itemize}
\item The \lstinline{delete} keyword is used to free objects allocated on the heap with \lstinline{new}
\item In C++11, you can also use \lstinline{delete} to specify a function as explicitly not existing.
\begin{lstlisting}[language=C++,basicstyle=\tiny\ttfamily]
int func(int x) = delete;
\end{lstlisting}
\item Can now also be used to explicitly deactivate methods which may be generated automatically.
\begin{lstlisting}[language=C++,basicstyle=\tiny\ttfamily]
class MyClass{
  public:
    MyClass(const MyClass& mc) = delete;
};
\end{lstlisting}
\item Aside: Note that you can also explicitly declare such a function as \lstinline{default}
\begin{lstlisting}[language=C++,basicstyle=\tiny\ttfamily]
class MyClass{
  public:
    MyClass(const MyClass& mc) = default;
};
\end{lstlisting}
\end{itemize}
\end{frame}

%\begin{frame}[fragile]
%\frametitle{Lambda Expressions}
%\begin{itemize}
%\item In many cases one can express code well using \emph{functional programming}
%% what is it?
%\item Users of MATLAB may be familiar with this practice
%\begin{lstlisting}[language=matlab]
%x = @(y) cos(y) + 1;
%\end{lstlisting}
%\item Mathematica lends itself nicely to functional programming
%\begin{lstlisting}[language=C++]
%
%\end{lstlisting}
%\item Some languages (LISP, Haskell, ..) are based around the idea
%\item C++11 introduces \emph{lambda expressions}
%\item There is a great deal to know about the details of these.
%% ..
%\end{itemize}
%\end{frame}

\begin{frame}[fragile]
\frametitle{\texttt{constexpr}}
\begin{itemize}
\item We commonly would like to evaluate some expression at compile time
\item C++11 allows you to evaluate expressions at compile time by declaring functions to be computed then.
\begin{lstlisting}[language=C++]
constexpr factorial (int x)
{
    return x>0 ? x*factorial(x - 1) : 1;
}
\end{lstlisting}
\item Restrictions include
\begin{itemize}
\item The function must have a single \lstinline{return} statement, and no other executable code (it can have \lstinline{typedef}s, \lstinline{std::static_assert}s, and \lstinline{using}s.
\item It can only call other \lstinline{constexpr} functions.
\item It obviously cannot depend on any data only known at runtime.
\end{itemize}
\item Template or preprocesser metaprogramming was used before, but these are fragile and confusing, and have limitations (for instance, you cannot template on a floating point value).
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{C++14}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{C++14}
\begin{itemize}
\item Intended to be a small upgrade of C++11, but does introduce several new features.
\item Automatic deduction of return type added 
\begin{lstlisting}[language=C++]
auto my_function();
\end{lstlisting}
\item The restrictions on \lstinline{constexpr} have been relaxed.
\item C++14 introduces \emph{variadic templates}
\begin{lstlisting}[language=C++]
template<typename T>
constexpr T Exp = T(2.7182818284590452353602);
\end{lstlisting}
\item Handy for numerics - \emph{digit separators}. Use \lstinline{'} anywhere.
\begin{lstlisting}[language=C++]
int x = 1'000'000'000; 
int y = 1'00'00'00'000; //in India, apparently
\end{lstlisting}
\item The next planned major upgrade will be C++17, with many large potential changes.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Final}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Assignment: Final}
Today, each student should present his or her plans for the final. 

See \texttt{final/Final.pdf}, and note that during the in-class final period on January 29th, you must give a \~{}10 minute presentation/demo of your project.
\end{frame}

\end{document}
