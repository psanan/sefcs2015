// Threading test

#include<iostream>
#include<vector>
#include<cmath>
#include<thread>
#include<chrono>
#include<future>

using std::cout;
using std::endl;

class ComputeEngine
{
  public: 
    void Initialize(size_t N);
    void TakeStep(); 
    double GetNorm() const;
    int GetCount() const;
    bool Done() const;
  
  private :
    std::vector<double> data_;
    mutable std::mutex data_lock_;

    std::atomic<int> count_;
};

// Note: this can also be done with a std::condition_variable
bool ComputeEngine::Done() const
{
  return GetCount() > 1000000;
}

int ComputeEngine::GetCount() const
{
  int r = count_;
  return r;
}

void ComputeEngine::Initialize(size_t N)
{
  // acquire mutex
  data_lock_.lock();

  data_.clear();
  data_.reserve(N);
  for(size_t i=0;i<N;++i){
    data_.push_back(double(i % 100));
  }

  // release mutex
  data_lock_.unlock();

}

void ComputeEngine::TakeStep()
{
  // acquire mutex
  data_lock_.lock();

  for(auto i=data_.begin(); i!=data_.end(); ++i){
      *i += 1e-4;
  }

  // release mutex
  data_lock_.unlock();

  count_++; // no lock since this is atomic!
}

double ComputeEngine::GetNorm() const
{
  // acquire mutex
  data_lock_.lock();

  double n = 0.0;
  for(auto i=data_.begin(); i!=data_.end(); ++i){
    n += (*i) * (*i);  
  }

  // release mutex
  data_lock_.unlock();

  n = sqrt(n);

  return(n);
}

void computeLoop(std::shared_ptr<ComputeEngine> ce)
{
  ce->Initialize(1023);
  while(!ce->Done()){
    ce->TakeStep();
    //cout << ce->GetCount() << endl;
  }
}

void monitorLoop(std::shared_ptr<ComputeEngine> ce)
{
  while(!ce->Done()){
    double norm = ce->GetNorm();
    int count = ce->GetCount();
    cout << "Monitor: count = " << count << ", norm =  " << norm << endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(300));
  }
}

int ComputeOther()
{
  int r = 10000;
  for(int i=0;i<100000;i++){
    r -= 1;
  }
  return r;
}

///////////////////////////////////////////////////////////////////////////////
int main()
{

  // This is some other piece of data we would like to process
  std::future<int> other = std::async(std::launch::async,&ComputeOther);

  std::shared_ptr<ComputeEngine> ce(new ComputeEngine());

  // On a separate thread, run the compute engine
  std::thread compute_thread(computeLoop,ce);

  // On another thread (really could be this thread) start the monitor
  std::thread monitor_thread(monitorLoop,ce);
  
  // Wait for both threads to terminate
  monitor_thread.join();
  compute_thread.join();

  // collect our other value
  cout << "Other value is " << other.get() << endl;
}
