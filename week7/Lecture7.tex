\input{"../templates/Lecture.tex.inc"}

\usepackage{listings}
\usepackage{color}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

%Overriding template
\author{Patrick Zulian}
\institute[USI Lugano] 
{
USI Lugano \\ 
\medskip
\textit{patrick.zulian@usi.ch}
}

\title[CEFCS Week 7]{Software Engineering for Computational Science: Week 7} 
\subtitle{Programming C++: Design Patterns}
\date{\today} 

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\titlepage 
\end{frame}
\begin{frame}
\tableofcontents 
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Design principles}
% \subsection*{Symptoms of poor design}
\begin{frame}
\frametitle{Symptoms of poor design}
\begin{itemize}
\item \textbf{Rigidity}: design is hard to change
\item \textbf{Fragility}: design is easy to break
\item \textbf{Immobility}: design is hard to reuse
\item \textbf{Needless complexity}: too many abstractions which are not used
\item \textbf{Needless repetition}: same code appears over and over again
\item \textbf{Opacity}: module becomes harder to understand over time
\item ...

\end{itemize}
\emph{...absence of design is certainly poor design}
\end{frame}
\begin{frame}
\frametitle{Remedies}
\begin{itemize}
\item \textbf{Single responsibility principle (SRP)}: A class should have only one reason to change
\item \textbf{Open-close principle (OCP)}: New functionalities means new adding new code not changing the old one
\item \textbf{Interface segregation principle (ISP)}: Clients should not be forced to depend on methods that they do not use
\item \textbf{Substitution principle (SP)}: Subtypes must be suitable for their base types (see Lecture 5)
\item \textbf{Dependency inversion principle (DIP)}:Abstractions should not depend on details. Details should depend on abstractions
\item For a more in-depth view see the \emph{Advanced programming and design} course by Prof. Walter Binder
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Design patterns}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Design patterns}
What are design patterns?
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Categories}
\begin{frame}
Here are some of the most popular patterns
\begin{table} \centering\footnotesize
\begin{tabular}{l|l|l}
\textbf{Creational} & \textbf{Structural} 	& \textbf{Behavioral} 	\\ \hline
Singleton  		  	&	Adapter		 		& 	Strategy 			\\
Factory method    	&	Decorator			&	Observer			\\
Abstract factory 	&	Composite			& 	Visitor				\\
Builder  		  	&	Proxy				&	Memento				\\
				  	&	Facade				& 	Iterator		 	\\
				  	&	Bridge				& 	Command		 		\\
\end{tabular}
\end{table}

\end{frame}

\subsection*{Class diagrams}
\begin{frame}
\begin{itemize}
\item The Object Modeling Technique (OMT) and the Unified Modeling Language (UML) provide a standards to visualize diagrams describing aspects of a software. 
\item In this lecture we will use the OMT
\item On-line references can be found at: 
	\begin{itemize}
	\item \url{https://msdn.microsoft.com/en-us/library/dd409437.aspx}
	\item \url{http://www.objectmentor.com/resources/articles/umlClassDiagrams.pdf}
	\item ...
	\end{itemize}
	\item The diagrams and definitions are taken from \cite{Gamma:1998:DPC:551551}.
\end{itemize}
\end{frame}

% The OMT notation for class inheritance is a triangle connecting a subclass (LineShape in the figure) to its parent class (Shape). 
%An object reference representing a part-of or aggregation relationship is indicated by an arrowheaded line with a diamond at the base. The arrow points to the class that is aggregated (e.g., Shape). 
%An arrowheaded line without the diamond denotes acquaintance (e.g., a LineShape keeps a reference to a Color object, which other shapes may share). A name for the reference may appear near the base to distinguish it from other references.2

\section{Creational patterns}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Singleton}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Singleton}
\textbf{Intent}: Ensure a class only has one instance, and provide a global point of access to it

\begin{figure}\centering\footnotesize
\includegraphics[width=0.5\linewidth]{figures/singleton}
\caption{Singleton}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Singleton in C++}]{listings/singleton.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Factory}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Factory method}
\textbf{Intent}: 
\begin{itemize}
\item Define an interface for creating an object, but let subclasses decide which class to instantiate 
\item Factory Method lets a class defer instantiation to subclasses
\end{itemize}

\begin{figure}\centering\footnotesize
\includegraphics[width=0.7\linewidth]{figures/factory_method}
\caption{Factory method}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Factory method in C++}]{listings/factory_method.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Abstract factory}
\textbf{Intent}: Provide an interface for creating families of related or dependent objects without specifying their concrete classes

\begin{figure}\centering\footnotesize
\includegraphics[width=0.7\linewidth]{figures/abstract_factory}
\caption{Abstract factory}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Abstract factory in C++}]{listings/abstract_factory.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection*{Builder}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Builder}
\textbf{Intent}: Separate the construction of a complex object from its representation so that the same construction process can create different representation
\begin{figure}\centering\footnotesize
\includegraphics[width=0.7\linewidth]{figures/builder}
\caption{Builder}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Builder in C++}]{listings/builder.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={HTML Builder}]{listings/html_builder.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={JSON Builder}]{listings/json_builder.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Builder: example usage}]{listings/usage_builder.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Structural patterns}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Adapter}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Adapter}
\textbf{Intent}: 
\begin{itemize}
\item Convert the interface of a class into another interface clients expect
\item Adapter lets classes work together that couldn't otherwise because of incompatible interfaces
\end{itemize}
\begin{figure}\centering\footnotesize
\includegraphics[width=0.7\linewidth]{figures/adapter}
\caption{Adapter}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Adapter in C++}]{listings/adapter.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection*{Proxy}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Proxy}
\textbf{Intent}: Provide a surrogate or placeholder for another object to control access to it
\begin{figure}\centering\footnotesize
\includegraphics[width=0.7\linewidth]{figures/proxy}
\caption{Proxy}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Proxy in C++}]{listings/proxy.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Proxy: example usage}]{listings/proxy_usage.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Decorator}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Decorator}
\textbf{Intent}: 
\begin{itemize}
\item Attach additional responsibilities to an object dynamically
\item Decorators provide a flexible alternative to subclassing for extending functionality
\end{itemize}
\begin{figure}\centering\footnotesize
\includegraphics[width=0.7\linewidth]{figures/decorator}
\caption{Decorator}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Decorator in C++}]{listings/decorator.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Decorator: example usage}]{listings/decorator_all.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Decorator: example usage}]{listings/decorator_usage.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Behavioral patterns}
\subsection*{Strategy}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Strategy}
\textbf{Intent}: 
\begin{itemize}
\item Define a family of algorithms, encapsulate each one, and make them interchangeable
\item Strategy lets the algorithm vary independently from clients that use it
\end{itemize}
\begin{figure}\centering\footnotesize
\includegraphics[width=0.7\linewidth]{figures/strategy}
\caption{Strategy}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Strategy in C++}]{listings/strategy.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Observer}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Observer}
\textbf{Intent}: Define a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically

\begin{figure}\centering\footnotesize
\includegraphics[width=0.7\linewidth]{figures/observer}
\caption{Observer}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Observer in C++}]{listings/observer.hpp}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Observer}]{listings/observer_usage.hpp}
\end{frame}

\section{Mixing patterns (Visitor and Composite)}

\begin{frame}
\frametitle{Visitor}
\textbf{Intent}: 
\begin{itemize}
\item Represent an operation to be performed on the elements of an object structure 
\item Visitor lets you define a new operation without changing the classes of the elements on which it operates
\end{itemize}
\begin{figure}\centering\footnotesize
\includegraphics[width=0.5\linewidth]{figures/visitor}
\caption{Visitor}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Composite}
\textbf{Intent}: 
\begin{itemize}
\item Compose objects into tree structures to represent part-whole hierarchies
\item Composite lets clients treat individual objects and compositions of objects uniformly
\end{itemize}
\begin{figure}\centering\footnotesize
\includegraphics[width=0.7\linewidth]{figures/composite}
\caption{Composite}
\end{figure}
\end{frame}



\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Visitor and composite in C++}]{listings/visitor_and_composite_base.hpp}
\end{frame}


\begin{frame}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen},
caption={Visitor and composite in C++}]{listings/visitor_and_composite.hpp}
\end{frame}

\section{In class exercise}
\begin{frame}
\frametitle{In class exercise}
Create a simple expression tree evaluator
\begin{itemize}
\item Use \textbf{Composite}, \textbf{Visitor} and \textbf{Adapter} patterns
\item Complete the following code snippet:
\end{itemize}
\lstinputlisting[language=C++,
basicstyle=\tiny\ttfamily,
commentstyle=\color{mygreen}]{in_class_exercise.cpp}
\end{frame}

\bibliographystyle{alpha}
\bibliography{biblio.bib}
\end{document}
