class Branch : public Node {
public:
	void accept(Visitor &visitor) {
		visitor.visit(*this);
		for(auto nodeptr : nodes_) { nodeptr->accept(visitor); }
	}

	void addChild(const shared_ptr<Node> &nodeptr) { nodes_.push_back(nodeptr); }

private:
	vector< shared_ptr<Node> > nodes_;
};

class Leaf : public Node {
public:
	void accept(Visitor &visitor) { visitor.visit(*this); }
};

auto root_ptr = make_shared<Branch>();
auto branch_ptr = make_shared<Branch>();

branch_ptr->addChild(make_shared<Leaf>());
branch_ptr->addChild(make_shared<Leaf>());
branch_ptr->addChild(make_shared<Leaf>());
root_ptr->addChild(branch_ptr);

Visitor visit;
root_ptr->accept(visit); //what is the output?


