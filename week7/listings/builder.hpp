class StringOutputBuilder {
public:
	virtual void start() {}
	virtual void finish() {}
	virtual void build(const string &key, const int value) = 0;	
	virtual void build(const string &key, const string &value) = 0;
	virtual string get_string() const = 0;

	virtual ~StringOutputBuilder() {}
};

string build_stuff(StringOutputBuilder &builder) {
	builder.start();
	builder.build("simulation_name", "Heat equation");
	builder.build("n_iterations", 100);
	builder.build("integration_method", "Explicit euler");
	builder.finish();
	return builder.get_string();
}