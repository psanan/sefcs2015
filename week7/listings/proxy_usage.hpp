
template<class BigDataT>
void do_stuff_with_big_data(const std::vector<std::string> &paths)
{
	using namespace std;

	vector< shared_ptr<BigDataT> > data;
	for(auto const &path : paths) {
		data.push_back(make_shared<BigDataT>(path));
	}

	//show only last
	data.back()->show();
}

//What happens? What is the output?
vector<string> paths = { "1MB.data", "2GB.data", "4KB.data" };

do_stuff_with_big_data<BigData>(paths);     	//1
do_stuff_with_big_data<BigDataProxy>(paths);	//2