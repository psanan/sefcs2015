class Strategy {
public: virtual void perform_task() = 0; /*...*/
};

class AClass {
public:
	AClass(const shared_ptr<Strategy> &strategy) : strategy_(strategy) {}
	void method_which_depends_on_task() { strategy_->perform_task(); }
private:
	shared_ptr<Strategy> strategy_;
};

class PerformTaskInAWay : public Strategy {
public: void perform_task()	{ printf("PerformTaskInAWay"); }
};

class PerformTaskInAnotherWay : public Strategy {
public: void perform_task()	{ printf("PerformTaskInAnotherWay"); }
};

void do_stuff(AClass &a) { a.method_which_depends_on_task(); }


AClass a1(make_shared<PerformTaskInAWay>());
do_stuff(a1);

AClass a2(make_shared<PerformTaskInAnotherWay>());
do_stuff(a2); 