class Filter {
public:
	virtual double apply(double) const = 0;
	virtual ~Filter() {}
};

class Identity : public Filter {
public:
	double apply(double value) const { return value; }
};

template<class Iterator>
void filter(const Iterator &begin, const Iterator &end, const Filter &f) {
	for(Iterator it = begin; it != end; ++it) { 
		*it = f.apply(*it); 
	}
}