class BigData {
public:
	BigData(const string &path) { read_from_file(path); }
	void show() { /* Displaying big data */ }
private:
	void read_from_file(const string &path) {
		cout << "Reading from file: " << path << endl;
		//reading ...
	}
};

class BigDataProxy {
public:
	BigDataProxy(const string &path) : path_(path) {}

	void show() {
		if(!data_) {
			data_ = make_shared<BigData>(path_);
		}

		data_->show();
	}

private:
	string path_;
	shared_ptr<BigData> data_;
};