
class FilterDecorator : public Filter {
public:
	FilterDecorator(const shared_ptr<Filter> &filter = make_shared<Identity>()) : filter_(filter) {}
	virtual ~FilterDecorator(){}
	const Filter &filter() const { return *filter_; }

private:
	shared_ptr<Filter> filter_;
};

class Twice : public FilterDecorator {
public:
	using FilterDecorator::FilterDecorator;

	double apply(double value) const { return filter().apply(value) * 2; }
};

template<int Expon>
class Pow : public FilterDecorator {
public:
	using FilterDecorator::FilterDecorator;
	double apply(double value) const { return pow(filter().apply(value), Expon); }
};