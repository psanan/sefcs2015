class IntegratorObserver {
public:
	virtual void solution_updated(const vector<double> &solution) = 0;
	virtual ~IntegratorObserver() {}
};

class PrintIntegratorObserver : public IntegratorObserver {
public:
	void solution_updated(const vector<double> &solution) override {
		for(auto v : solution) { cout << setprecision(3) << v << "\t "; }
		cout << endl;
	}
};

class Integrator {
public:
	void add_observer(const shared_ptr<IntegratorObserver> &observer) { observers_.push_back(observer); }

	void fire_solution_updated(const vector<double> &solution) {
		for(auto obs_ptr : observers_) { obs_ptr->solution_updated(solution); }
	} 

private:
	vector< shared_ptr<IntegratorObserver> > observers_;
};