class Singleton {
public:
	static Singleton &instance() {
		std::cout << "I am here" << std::endl;
		static Singleton instance;
		return instance;
	}

private:
	Singleton() { std::cout << "I\'am constructed." << std::endl; }
};

//What is printed here?
Singleton::instance(); //1 
Singleton::instance(); //2
Singleton::instance(); //3 
