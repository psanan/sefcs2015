class Interface {
public:
	virtual void do_something() = 0;
	virtual ~Interface(){}
};

void some_algorithm(Interface &in) { in.do_something(); }

class ExternalLibraryObject {
public:
	void do_stuff() {}
};

class Adapter : public Interface {
public:
	explicit Adapter(ExternalLibraryObject &obj) : obj_(obj) {}

	void do_something() override { obj_.do_stuff(); }

private:
	ExternalLibraryObject &obj_;
};


ExternalLibraryObject object;
Adapter adapted_object(object);
some_algorithm(adapted_object);