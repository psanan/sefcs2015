JSONOutputBuilder json;
HTMLOutputBuilder html;

cout << "---------------   JSON  ---------------------" << endl;
cout << build_stuff(json) << endl;
cout << "---------------   HTML  ---------------------" << endl;
cout << build_stuff(html) << endl;
cout << "-------------------------------------------" << endl;