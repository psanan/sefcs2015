class Object { public: virtual ~Object() {} };
class MyObject : public Object {};

class Application {
public:
	virtual shared_ptr<Object> create_object() = 0;
	virtual ~Application() {}
};

class MyApplication : public Application {
public:
	shared_ptr<Object> create_object() { return make_shared<MyObject>(); }
};

shared_ptr<Application> app = make_shared<MyApplication>();
shared_ptr<Object> obj = app->create_object();