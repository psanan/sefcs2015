class Visitor;
class Branch;
class Leaf;

class Node {
public:
	virtual void accept(Visitor &visit) = 0;
	virtual ~Node() {}
};

class Visitor {
public:
	void visit(Leaf &) {
		cout << "Doing something with a Leaf" << endl;
	}

	void visit(Branch &) {
		cout << "Doing something with a Branch" << endl;
	}
};