template<class Iterator>
void filter(const Iterator &begin, const Iterator &end, const Filter &f) {
	for(Iterator it = begin; it != end; ++it) { 
		*it = f.apply(*it); 
	}
}

vector<double> values = {1., 2., 3.};
shared_ptr<Filter> f = make_shared<Twice>(make_shared< Pow<2> >());

filter(values.begin(), values.end(), *f);

//What is it output?
for(double v : values) { 
	cout << v  << " " << endl; 
}