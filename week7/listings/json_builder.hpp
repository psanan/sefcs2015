class JSONOutputBuilder : public StringOutputBuilder {
public:
	void start() override { stream_.clear(); stream_ << "{ "; }

	void build(const string &key, const int value) override {
		stream_ << "\n\t" << key << " : " << value << ",";
	}	

	void build(const string &key, const string &value) override {
		stream_ << "\n\t" << key << " : \"" << value << "\",";
	}

	void finish() override { stream_.seekp(-1,stream_.cur); stream_ << "\n}\n"; }
	
	string get_string() const { return stream_.str(); }

private:
	stringstream stream_;
};