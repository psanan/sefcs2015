class HTMLOutputBuilder : public StringOutputBuilder {
public:
	void start() override { stream_.clear(); stream_ << "<!DOCTYPE html><html><body><ul>\n"; }

	void build(const string &key, const int value) override {
		stream_ << "\t<li><b>" << key << "</b>:" << value << "</li>\n";
	}	

	void build(const string &key, const string &value) override {
		stream_ << "\t<li><b>" << key << "</b>:" << value << "</li>\n";
	}
	
	void finish() override { stream_ << "</ul></body></html>"; }

	string get_string() const { return stream_.str(); }

private:
	stringstream stream_;
};