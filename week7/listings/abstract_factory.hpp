
class Algorithm { public: virtual void compute() = 0; /*...*/ };

class Serial   : public Algorithm { void compute() { printf("Slow alg"); } };
class Parallel : public Algorithm { void compute() { printf("Fast alg"); } };

class AlgFactory {
	virtual shared_ptr<Algorithm> create_pde_solver() = 0;
	virtual shared_ptr<Algorithm> create_ode_solver() = 0;
	virtual ~AlgFactory() {}
};

class MyAlgFactory : public AlgFactory {
	shared_ptr<Algorithm> create_pde_solver() { return make_shared<Parallel>(); }
	shared_ptr<Algorithm> create_ode_solver() { return make_shared<Serial>(); }
};

class MyApplication {
	shared_ptr<AlgFactory> get_algs() { return make_shared<MyAlgFactory>(); }
};

shared_ptr<MyApplication> app = make_shared<MyApplication>();
auto factory = app->get_algs(); //the factory is usually passed to some function
auto pde_algorithm = factory->create_pde_solver();
auto ode_algorithm = factory->create_ode_solver();
//Now we can do stuff with the algorithms