class ExplicitEuler : public Integrator {
public:
	template<class Function>
	void integrate(vector<double> &solution, double dt, size_t n_steps, Function fun) {

		const size_t n = solution.size();
		vector<double> update;
		for(size_t t = 0; t != n_steps; ++t) {
			update = fun(solution);
			for(size_t i = 0; i != n; ++i) {
				solution[i] += dt * update[i];
			}
			fire_solution_updated(solution);
		}
	}
};

ExplicitEuler euler;
euler.add_observer(make_shared<PrintIntegratorObserver>());
const size_t n = 10;
vector<double> solution(n, 1);
euler.integrate(solution, 0.1, 5, 
	[n](const vector<double> &solution) -> vector<double> {
		vector<double> result(solution.size());
		for(size_t i = 0; i != n; ++i) {
			result[i] = -solution[i]*0.1;
		}
		return move(result);
	});