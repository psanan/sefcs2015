double a = 0.5;
double x = 3000;
double y = 15;

//Create wrappers (adapter)
shared_ptr<Expression> a_expr = make_shared<Value>(a); 
shared_ptr<Expression> x_expr = make_shared<Value>(x);
shared_ptr<Expression> y_expr = make_shared<Value>(y);

//Compose expression (composite)
shared_ptr<Expression> a_times_x = make_shared<Multiply>(a_expr, x_expr);
shared_ptr<Expression> a_times_x_plus_y = make_shared<Add>(a_times_x, y_expr);
	
//Visit tree (visitor)
Evaluator evaluator;
a_times_x_plus_y->accept(evaluator);

//Get result
cout << evaluator.result() << endl;