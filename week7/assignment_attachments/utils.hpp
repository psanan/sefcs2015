#ifndef UTILS_HPP
#define UTILS_HPP 

#include <ostream>
#include <string>

//Your Grid class
#include "Grid.hpp"

namespace sefcs {
	///Writes the grid to the output stream as a 2D matrix of values
	void describe(const Grid &grid, std::ostream &os);

	///Writes to grid to a file at a given path
	void write_to_file(const Grid &grid, const std::string &path);

	//Use this to write your timesteps
	//Writes the grid to a file such that the path has the correct prefix for being sorted ordinally
	void write_to_file(const Grid &grid, const long step, const long n_timesteps);
}

#endif //UTILS_HPP
