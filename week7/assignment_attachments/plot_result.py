from numpy.random import uniform, seed
from matplotlib.mlab import griddata
from mpl_toolkits.axes_grid1 import ImageGrid
import matplotlib.pyplot as plt
import numpy as np
import glob
import matplotlib.animation as animation

steps = glob.glob("./sim_files/sim_*.txt")
fig = plt.figure()

with open(steps[0]) as file:
	array2d = [[float(digit) for digit in line.split()] for line in file]

im = plt.imshow(array2d, cmap=plt.get_cmap('jet'))
plt.clim()
plt.colorbar()
plt.title("Simulation t=0");
i = 0;
def updatefig(*args):
	global i
	if i < len(steps):
		with open(steps[i]) as file:
			array2d = [[float(digit) for digit in line.split()] for line in file]
		
		im.set_data(array2d);
		plt.title("Simulation t_step=%d" % i);
		i+=1
	else:
		exit()
	
	return im,


ani = animation.FuncAnimation(fig, updatefig, interval=50)
plt.show()
