#include "utils.hpp"

#include <cmath>
#include <fstream>

namespace sefcs {
	void describe(const Grid &grid, std::ostream &os) {
		//IMPLEMENT ME
	}

	void write_to_file(const Grid &grid, const std::string &path) {
		std::ofstream os(path);
		if(!os.good()) {
			os.close();
			return;
		}

		describe(grid, os);
		os.close();
	}

	void write_to_file(const Grid &grid, const long step, const long n_timesteps) {
		using std::log10;
		const long max =     ceil(log10(1 + n_timesteps));
		const long current = ceil(log10(1 + step));

		std::string zeros;
		zeros.assign(max-current, '0');

		write_to_file(grid, "sim_"+ zeros  + std::to_string(step) + ".txt");
	}
}
