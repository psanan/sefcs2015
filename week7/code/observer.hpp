#ifndef OBSERVER_HPP
#define OBSERVER_HPP 

#include <memory>
#include <vector>
#include <iostream>
#include <iomanip>

namespace sefcs {

	class IntegratorObserver {
	public:
		virtual void solution_updated(const std::vector<double> &solution) = 0;
		virtual ~IntegratorObserver() {}
	};

	class PrintIntegratorObserver : public IntegratorObserver {
	public:
		virtual void solution_updated(const std::vector<double> &solution) override
		{

			for(auto v : solution) {
				std::cout << std::setprecision(3);
				std::cout << v <<"\t ";
			}

			std::cout << std::endl;
		}
	};

	class Integrator {
	public:
		void add_observer(const std::shared_ptr<IntegratorObserver> &observer)
		{
			observers_.push_back(observer);
		}

		void fire_solution_updated(const std::vector<double> &solution) {
			for(auto obs_ptr : observers_) {
				obs_ptr->solution_updated(solution);
			}
		} 

	private:
		std::vector< std::shared_ptr<IntegratorObserver> > observers_;
	};

	class ExplicitEuler : public Integrator {
	public:
		template<class Function>
		void integrate(std::vector<double> &solution, const double dt, const long n_timesteps, Function fun) {

			const long n = solution.size();
			std::vector<double> update;
			for(long t = 0; t < n_timesteps; ++t) {
				update = fun(solution);
				for(long i = 0; i < n; ++i) {
					solution[i] += dt * update[i];
				}

				fire_solution_updated(solution);
			}
		}
	};

	void observer_example()
	{
		using namespace std;

		ExplicitEuler euler;
		auto printer = make_shared<PrintIntegratorObserver>();
		euler.add_observer(printer);

		const long n = 10;
		vector<double> solution(n, 1);
		euler.integrate(solution, 0.1, 5, [n](const vector<double> &solution) -> vector<double> {
			vector<double> result(solution.size());
			for(long i = 0; i < n; ++i) {
				result[i] = -solution[i]*0.1;
			}

			return result;
		});
	}
}

#endif //OBSERVER_HPP
