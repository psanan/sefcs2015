#ifndef VISITOR_HPP
#define VISITOR_HPP 

//Example of visitor and composite patterns
namespace sefcs {
	//Forward declarations
	class Visitor;
	class Branch;
	class Leaf;

	class Node {
	public:
		virtual void accept(Visitor &visit) = 0;
		virtual ~Node() {}
	};

	class Visitor {
	public:
		void visit(Leaf &) {
			std::cout << "Doing something with a Leaf" << std::endl;
		}

		void visit(Branch &) {
			std::cout << "Doing something with a Branch" << std::endl;
		}
	};


	class Branch : public Node {
	public:
		void accept(Visitor &visitor) 
		{
			visitor.visit(*this);

			for(auto node_ptr : children_) {
				node_ptr->accept(visitor);
			}
		}

		void addChild(const std::shared_ptr<Node> &node_ptr)
		{
			children_.push_back(node_ptr);
		}

	private:
		std::vector< std::shared_ptr<Node> > children_;
	};

	class Leaf : public Node {
	public:
		void accept(Visitor &visitor) 
		{
			visitor.visit(*this);
		}
	};

	void visitor_example()
	{
		using std::make_shared;
		auto root_ptr = make_shared<Branch>();
		auto branch_ptr = make_shared<Branch>();

		branch_ptr->addChild(make_shared<Leaf>());
		branch_ptr->addChild(make_shared<Leaf>());
		branch_ptr->addChild(make_shared<Leaf>());

		root_ptr->addChild(branch_ptr);

		Visitor visit;
		root_ptr->accept(visit);

	}
}

#endif //VISITOR_HPP
