#ifndef SINGLETON_HPP
#define SINGLETON_HPP 
#include <iostream>

namespace sefcs {
	class Singleton {
	public:
		static Singleton &instance() {
			std::cout << "I am here" << std::endl;
			static Singleton instance;
			return instance;
		}

	private:
		Singleton()
		{
			std::cout << "I\'am constructed." << std::endl;
		}
	};

	void singleton_example()
	{
		Singleton::instance();
		Singleton::instance();
		Singleton::instance();
	}
}

#endif //SINGLETON_HPP

