#ifndef STRATEGY_HPP
#define STRATEGY_HPP 

namespace sefcs {
	class Strategy {
	public:
		virtual void perform_task() = 0;
		virtual ~Strategy() {}
	};

	class AClassWithSomeInnerComputation {
	public:
		AClassWithSomeInnerComputation(const std::shared_ptr<Strategy> &strategy)
		: strategy_(strategy) {}

		virtual void method_which_depends_on_task() {
			//Do stuff...
			strategy_->perform_task();
			//Do other stuff...
		}

	private:
		std::shared_ptr<Strategy> strategy_;
	};

	class PerformTaskInAWay : public Strategy {
	public:
		void perform_task() 
		{
		 	std::cout << "PerformTaskInAWay" << std::endl;
		}
	};

	class PerformTaskInAnotherWay : public Strategy {
	public:
		void perform_task() 
		{
		 	std::cout << "PerformTaskInAnotherWay" << std::endl;
		}
	};

	void do_stuff(AClassWithSomeInnerComputation &a)
	{
		//here the task is performed using a particular strategy
		a.method_which_depends_on_task();
	}

	void strategy_example() {

		AClassWithSomeInnerComputation a1(std::make_shared<PerformTaskInAWay>());
		do_stuff(a1);

		AClassWithSomeInnerComputation a2(std::make_shared<PerformTaskInAnotherWay>());
		do_stuff(a2); 
	}
}

#endif //STRATEGY_HPP

