#include "adapter.hpp"
#include "builder.hpp"
#include "decorator.hpp"
#include "factory.hpp"
#include "observer.hpp"
#include "singleton.hpp"
#include "visitor_and_composite.hpp"
#include "strategy.hpp"
#include "proxy.hpp"

#include <stdlib.h>

int main(const int argc, const char *argv[])
{
	using std::cout;
	using std::endl;

	using namespace sefcs;
	(void) argc;
	(void) argv;

	cout << "\n--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#\n" << endl;
	cout << "Adapter: " << endl;
	adapter_example();
	cout << "\n--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#\n" << endl;
	cout << "Decorator: " << endl;
	decorator_example();
	cout << "\n--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#\n" << endl;
	cout << "Builder: " << endl;
	builder_example();
	cout << "\n--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#\n" << endl;
	cout << "Observer: " << endl;
	observer_example();
	cout << "\n--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#\n" << endl;
	cout << "Singleton: " << endl;
	singleton_example();
	cout << "\n--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#\n" << endl;
	cout << "Factory: " << endl;
	factory_example();
	cout << "\n--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#\n" << endl;
	cout << "Visitor: " << endl;
	visitor_example();
	cout << "\n--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#\n" << endl;
	cout << "Strategy: " << endl;
	strategy_example();
	cout << "\n--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#\n" << endl;
	cout << "Proxy: " << endl;
	proxy_example();
	cout << "\n--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#--#\n" << endl;
	return EXIT_SUCCESS;
}