#ifndef BUILDER_HPP
#define BUILDER_HPP
#include <string>
#include <sstream>
#include <iostream>

namespace sefcs {

	class StringOutputBuilder {
	public:
		virtual void start() {}
		virtual void finish() {}
		virtual void build(const std::string &key, const int value) = 0;	
		virtual void build(const std::string &key, const std::string &value) = 0;
		virtual std::string get_string() const = 0;

		virtual ~StringOutputBuilder() {}
	};

	class JSONOutputBuilder : public StringOutputBuilder {
	public:
		inline void start() override
		{
			stream_.clear();
			stream_ << "{ ";
		}

		inline void build(const std::string &key, const int value) override
		{
			stream_ << "\n\t" << key << " : " << value << ",";
		}	

		inline void build(const std::string &key, const std::string &value) override
		{
			stream_ << "\n\t" << key << " : \"" << value << "\",";
		}

		inline void finish() override
		{
			stream_.seekp(-1,stream_.cur);
			stream_ << "\n}\n";
		}
		
		inline std::string get_string() const 
		{
			return stream_.str();
		}

	private:
		std::stringstream stream_;
	};

	class HTMLOutputBuilder : public StringOutputBuilder {
	public:
		inline void start() override
		{
			stream_.clear();
			stream_ << "<!DOCTYPE html><html><body><ul>\n";
		}

		inline void build(const std::string &key, const int value) override
		{
			stream_ << "\t<li><b>" << key << "</b>:" << value << "</li>\n";
		}	

		inline void build(const std::string &key, const std::string &value) override
		{
			stream_ << "\t<li><b>" << key << "</b>:" << value << "</li>\n";
		}
		
		inline void finish() override
		{
			stream_ << "</ul></body></html>";

		}

		inline std::string get_string() const 
		{
			return stream_.str();
		}

	private:
		std::stringstream stream_;
	};

	std::string build_stuff(StringOutputBuilder &builder) {
		builder.start();
		builder.build("simulation_name", "Heat equation");
		builder.build("n_iterations", 100);
		builder.build("integration_method", "Explicit euler");
		builder.finish();
		return builder.get_string();
	}

	void builder_example()
	{
		using std::cout;
		using std::endl;

		JSONOutputBuilder json;
		HTMLOutputBuilder html;

		cout << "---------------   JSON  ---------------------" << endl;
		cout << build_stuff(json) << endl;
		cout << "---------------   HTML  ---------------------" << endl;
		cout << build_stuff(html) << endl;
		cout << "-------------------------------------------" << endl;
	}
}

#endif //BUILDER_HPP

