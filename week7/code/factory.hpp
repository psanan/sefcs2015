#ifndef FACTORY_HPP
#define FACTORY_HPP 

#include <memory>

namespace sefcs {
	//Abstract factory
	class Algorithm {
	public:
		virtual void compute() = 0;
		virtual ~Algorithm() {}
	};

	class SerialAlgorithm : public Algorithm {
	public:
		virtual void compute() 
		{
			std::cout << "Really slow serial computation" << std::endl;
		}
	};

	class ParallelAlgorithm : public Algorithm {
	public:
		virtual void compute() override
		{
			std::cout << "Really fast parallel computation" << std::endl;
		}
	};

	class AlgorithmFactory {
	public:
		virtual std::shared_ptr<Algorithm> pde_solver() = 0;
		virtual std::shared_ptr<Algorithm> ode_solver() = 0;
		virtual ~AlgorithmFactory() {}
		
	};

	class MyAlgorithmFactory : public AlgorithmFactory {
	public:
		std::shared_ptr<Algorithm> pde_solver() override
		{
			return std::make_shared<ParallelAlgorithm>();
		}

		std::shared_ptr<Algorithm> ode_solver() override
		{
			return std::make_shared<SerialAlgorithm>();
		}
	};

	//Factory method: get_algorithms
	class Application {
	public:
		virtual std::shared_ptr<AlgorithmFactory> get_algorithms() = 0;
		virtual ~Application() {}
	};

	class MyApplication : public Application {
	public:
		std::shared_ptr<AlgorithmFactory> get_algorithms()
		{
			return std::make_shared<MyAlgorithmFactory>();
		}
	};

	//A bit perverse example. We have a factory method returning an abstract factory
	void factory_example()
	{
		using namespace std;

		shared_ptr<Application> app = make_shared<MyApplication>();
		//factory method
		auto factory = app->get_algorithms();

		//abstract factory
		auto pde_algorithm = factory->pde_solver();
		auto ode_algorithm = factory->ode_solver();

		pde_algorithm->compute();
		ode_algorithm->compute();
	}
}

#endif //FACTORY_HPP

