#ifndef PROXY_HPP
#define PROXY_HPP 

namespace sefcs {
	class BigData {
	public:

		BigData(const std::string &path) 
		{
			read_from_file(path);
		}

		void show()
		{
			std::cout << "Show big data" << std::endl;	
		}
	private:
		void read_from_file(const std::string &path) 
		{
			std::cout << "Reading from file: " << path << std::endl;
		}
	};

	class BigDataProxy {
	public:
		BigDataProxy(const std::string &path) 
		: path_(path) {}

		void show() {
			if(!data_) {
				data_ = std::make_shared<BigData>(path_);
			}

			data_->show();
		}

	private:
		std::string path_;
		std::shared_ptr<BigData> data_;
	};

	template<class BigDataT>
	void do_stuff_with_big_data(const std::vector<std::string> &paths)
	{
		using namespace std;

		vector< shared_ptr<BigDataT> > data;
		for(auto const &path : paths) {
			data.push_back(make_shared<BigDataT>(path));
		}

		//show only last
		data.back()->show();
	}

	void proxy_example()
	{
		using namespace std;

		vector<string> paths = { "1MB.data", "2GB.data", "4KB.data" };

		cout << "------------------- DATA CLASS ------------------" << endl;
		do_stuff_with_big_data<BigData>(paths);

		cout << "------------------- PROXY CLASS ------------------" << endl;
		do_stuff_with_big_data<BigDataProxy>(paths);

	}
}

#endif //PROXY_HPP

