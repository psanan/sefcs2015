#ifndef DECORATOR_H
#define DECORATOR_H

#include <memory>
#include <vector>
#include <iostream>
#include <cmath>

namespace sefcs {
	class Filter {
	public:
		virtual double apply(const double) const = 0;
		virtual ~Filter() {}
	};

	class Identity : public Filter {
	public:
		inline double apply(const double value) const override
		{
			return value;
		}
	};

	class FilterDecorator : public Filter {
	public:
		FilterDecorator(const std::shared_ptr<Filter> &filter = std::make_shared<Identity>())
		: filter_(filter)
		{}

		virtual ~FilterDecorator(){}

		inline const Filter &filter() const
		{
			return *filter_;
		}

	private:
		std::shared_ptr<Filter> filter_;
	};

	class Twice : public FilterDecorator {
	public:
		using FilterDecorator::FilterDecorator;

		inline double apply(const double value) const override
		{
			return filter().apply(value) * 2;
		}
	};

	template<int Exponent>
	class Pow : public FilterDecorator {
	public:
		using FilterDecorator::FilterDecorator;
		inline double apply(const double value) const override
		{
			return pow(filter().apply(value), Exponent);
		}
	};

	template<class Iterator>
	void filter(const Iterator &begin, const Iterator &end, const Filter &f)
	{
		for(Iterator it = begin; it != end; ++it) {
			*it = f.apply(*it);
		}
	}

	void decorator_example()
	{
		using std::shared_ptr;
		using std::make_shared;

		std::vector<double> values = {1., 2., 3.};

		shared_ptr<Filter> f = make_shared<Twice>(make_shared< Pow<2> >());
		filter(values.begin(), values.end(), *f);

		for(double v : values) {
			std::cout << v  << " " << std::endl;
		}
	}
}

#endif //DECORATOR_H

