EXNAME=main
OBJECTS=main.o solver.o
LIBS=-lawesome
INCLUDE=-I../awesomelib/include
CXX=g++
CXXFLAGS=-Wall -Wpedantic

all : $(EXNAME)

$(EXNAME) : $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(LIBS) -o $@ $^

%.o : %.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c -o $@ $^

clean :
	rm -f $(EXNAME) $(OBJECTS)

.PHONY : clean all
