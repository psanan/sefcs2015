\input{"../templates/Lecture.tex.inc"}

\title[CEFCS Week 3]{Software Engineering for Computational Science: Week 3} 
\subtitle{Getting Help, Introduction to C++, Makefiles, and More Tools}
\date{September 30, 2015} 

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\titlepage 
\end{frame}

\begin{frame}
\tableofcontents 
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Getting Help}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Getting Help}
At this point in the course, you will probably have realized that programming requires you to quickly answer many questions, on many levels.
\begin{itemize}
\item Language syntax
\item Tool usage
\item Algorithm choice
\item Debugging
\item Other people's code
\item ..
\end{itemize}
Learning how to answer these questions quickly and definitively is essential.
\end{frame}

\begin{frame}
\frametitle{Getting Help}
\begin{itemize}
\item Find good resources. For C and C++ I have recommended some standard texts, but there are thousand of references, so if you find one you like, continue to use it.
\item Consider documenting your solutions to head-scratching problems in a searchable way.
\begin{itemize}
\item Software development is sometimes (frustratingly) an experimental science. Keep a lab notebook like lab scientists do.
\item An easy way to do this is with a blog. Make entries which detail how you solved various problems (include error message text!)
\end{itemize}
\item Practice the art of asking other people for help. The rest of this portion of the lecture will concern this.
\end{itemize}
\end{frame}

% Posing Questions
\begin{frame}[fragile]
\frametitle{How to Pose a Question}
\begin{enumerate}
\item What \emph{is} your question?
\begin{itemize}
\item What are my assumptions?
\end{itemize}
\item What is your goal? (What is the \emph{real} question?)
\begin{itemize}
\item The ``XY problem'' : Someone wants to accomplish X, and tries to do it with Y. Y doesn't work, so they ask how to get Y to work, when really they care about X.
\end{itemize}
\item Are there terms you don't understand?
\item Ask yourself a question: "Why can't I answer this myself?"
\end{enumerate}
We will use a simple example to demonstrate these points more clearly
\end{frame}

\begin{frame}[fragile]
\frametitle{An example}
\begin{itemize}
\item
I have been tasked to protype an application in C++. It is a FEM code, and fortunately my advisor has provided me with some reference code which accomplishes a similar task!
\item You copy the code, set up a local git repository to track your progress, and happily begin modifying it to your purposes.
\item At one point you want to prototype a local operation and, since you don't know Eigen, you write a little test script to understand how to add two small matrices together.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{An example}
\begin{center}
\scalebox{0.7}{
\lstinputlisting{eigenWoes/main.cpp}
}
\end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{An example}
\begin{itemize}
\item Suddenly you type \texttt{make} (more later) to rebuild your code and you are greeted with
\end{itemize}
\lstinputlisting[language=bash]{eigenWoes/error.txt}
\red{Now What?!}
\end{frame}

\begin{frame}[fragile]
\frametitle{Exercise: go over the standard questions}
\begin{enumerate}
\item What \emph{is} your question?
\begin{itemize}
\item What are my assumptions?
\end{itemize}
\item What is your goal? (What is the \emph{real} question?)
\item Are there terms you don't understand?
\item Why can't I answer this myself?
\end{enumerate}
\end{frame}

\begin{frame}[fragile]
\frametitle{How to ask for help}
Now that I have posed my question and decided that I can't answer it, it's time to look for outside help.
\begin{itemize}
\item Have I read the error message?
\item Is there documentation that I can understand? [If I can't understand it, why not?]
\item Search engines
\begin{itemize}
\item This is fairly obvious, but it's helpful to have understood your problem enough to provide good search terms.
\end{itemize}
\item Colleagues
\begin{itemize}
\item Often the most efficient way - no general advice here, as this depends strongly on the nature of the interpersonal relationship.
\end{itemize}
\item Mailing Lists
\item Q+A sites / message boards
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Mailing Lists}
When asking for help (for free) from a stranger, it's important to establish immediately that you are taking the interaction seriously, and that you value their time.
\begin{itemize}
\item Read the error message (and understand the terms).
\item Know the question you are asking. Refine it.
\item Due Diligence. Error Messages. Documentation. FAQ.
\item Write your email, and then edit it. 
\begin{itemize}
\item You are often writing because you are frustrated. Pretend to be calm.
\end{itemize}
\item Describe the environment. \textbf{Allow reproducibility}.
\begin{itemize}
\item Versions of software, compilers, and full error messages.
\item Code to reproduce, if at all possible
\item Question your assumptions.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Mailing Lists}
\begin{itemize}
\item Don't editorialize or draw premature conclusions. 
\begin{itemize}
\item Be very wary of claiming that you have found a bug. 
\item It is better to simply report your problem in a reproducible way. 
\end{itemize}

\item Don't be afraid! Software developers can seem unfriendly, but almost always this is the result of someone not doing their due diligence.
\begin{itemize}
\item
``Beginner's question'' $\ne$ ``Stupid Question''
\item ``Question with an easy-to-find answer'' = ``Stupid Question''
\item These lists are beneficial to the developers as well, and they are meant to be used - bugs are reported, user experience is reported.
\end{itemize}
\item Don't be offended by short answers, lack of salutations and other formalities, etc.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Q+A sites and message boards}
Most of the same points apply as with email, but a few more:
\begin{itemize}
\item You should already have identified unknown terminology, but now do your best to define the relevant terms to your problem. For example, ``Eigen'', ``Array'', ``C++'', ``comma initializer'', ``runtime error'', might be helpful for our example.
\item Due diligence. Search the site. Read the site's rules and FAQ.
\item Allow reproducibility. Give details of your environment, and code if at all possible.
\item Value the reader's time. Write clearly, and format nicely.
\item Post what worked for future readers.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Our example, continued}
Say that I am stil having problems with my code to add two matrices together.
\\
Exercise: Let's try to draft an email to ask for help.

\end{frame}

\begin{frame}[fragile]
\frametitle{Post-mortem}
\textbf{Learn from your mistakes}
\begin{itemize}
\item What was the actual solution my problem? (You may have tried many things)
\item What concept, if any, led to my confusion? 
\begin{itemize}
\item In our example, it was our confusion (perhaps born of \texttt{MATLAB}), that an \texttt{Array} is the same thing as a \texttt{Matrix}.
\item This is especially important if you ``solved'' your problem by googling the error message and copying whatever you found. Can you understand better, while the issue is fresh in your mind?
\end{itemize}
\item If this was difficult or time-consuming in any way, where can I write this down so that when I see this same error, I won't have to repeat all of this? 
\begin{itemize}
\item This is especially useful when dealing with large and complex systems which are being changed and updated 
\end{itemize}
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Tools}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Makefiles}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{The Problem}
\begin{itemize}
\item Once your project has more than a few files and translation units, it becomes slow and error prone to enter compiler commands yourself.
\item There is no need to remcompile every object if you have only changed the source code for one
\item a \emph{makefile} allows you to specify
\begin{itemize}
\item Commands to produce different files, be they executables, object files, libraries, or anything else
\item A set of dependencies
\end{itemize}
\item We will focus on GNU Make, which is supported practically everywhere
\item The \texttt{make} program uses a makefile and the modification date of the files mentioned in it to rebuild out-of-date files (only).
\begin{itemize}
\item See \texttt{https://www.gnu.org/software/make/} for documentation.
\end{itemize}
\item Later, we will mention \emph{build systems}, (CMake, for example)  which deal with the fact that makefiles aren't portable.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Makefiles}
\begin{itemize}
\item Makefiles are almost always named \texttt{makefile} or \texttt{Makefile}, since running \lstinline{make} with no arguments looks for those files (in that order).
\item You can run an arbitrary makefile with \lstinline{make -f another.make}
\item You can run \texttt{make} in another directory with \lstinline{make -C /path/to/somewhere}
\item You can define variables and use them later (don't forget the parentheses)
\begin{lstlisting}[language=C++]
VAR=value
ANOTHERVAR=$(VAR)
\end{lstlisting}
\item The rules are defined as
\begin{lstlisting}[language=C++]
target : prerequisites
  command	
\end{lstlisting}
\item Note that there must be a literal TAB character before \lstinline{command} here (not spaces)
%\item You can define certain targes as ``phony'', meaning that they do not name actual files.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Making Better Makefiles}
\begin{itemize}
\item
As with C and C++ code, try to avoid defining the same value in more than one place, since you will inevitably have to change it.
\item Okay makefile
\lstinputlisting{okay.makefile}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{A better makefile}
\lstinputlisting{better.makefile}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{More Unix Tools}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Grep}
\begin{itemize}
\item Pattern matching with regular expressions on a stream or file
\item The name comes from \lstinline{g/re/p}, old \texttt{ed} (similar to vim's command line) syntax
\item Search a file for matching lines
\begin{lstlisting}[language=C++]
grep *.makefile Lecture3.tex
\end{lstlisting}
\item Show some context
\begin{lstlisting}[language=C++]
grep -C 5 lstlisting Lecture3.tex
\end{lstlisting}
\item Search a stream
\begin{lstlisting}[language=C++]
ll | grep makefile
\end{lstlisting}
\item Search multiple files
\begin{lstlisting}[language=C++]
grep main *.makefile
grep -R main .
\end{lstlisting}
\end{itemize}
\end{frame}


\begin{frame}[fragile]
\frametitle{du}
\begin{itemize}
\item A helpful utility for determining sized of files in directories, in system blocks.
\begin{lstlisting}[language=C++]
du eigenWoes
\end{lstlisting}
\item The \texttt{-h} makes the output readable by \texttt{h}umans
\begin{lstlisting}[language=C++]
du -h eigenWoes
\end{lstlisting}
\item And the \texttt{-s} flag provides a summary (total)
\begin{lstlisting}[language=C++]
du -sh eigenWoes
\end{lstlisting}
(Note that by convention, single-letter options to Unix utilities can be combined. This is equivalent to \lstinline{du -s -h eigenWoes})
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{find}
\begin{itemize}
\item \texttt{find} is a quite general tool for looking for files
\item It has its own syntax (see \texttt{man find})
\item A common case is looking for a file in a directory (and its subdirectories)
\begin{lstlisting}[language=C++]
find [path] [expression]
find eigenWoes -name *.h
\end{lstlisting}
\item On OS X, you can use \texttt{mdfind}, which is a custom function which uses the spotlight index. This could be faster for searching large directory trees.
\begin{lstlisting}[language=C++]
mdfind -onlyin eigenWoes *.h
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{top, kill, ps}
\begin{itemize}
\item The operating system is responsible for running different \emph{processes} which are running programs, with memory available to them. 
\item These are identified by numbers call \emph{process ids}, and are owned by a user.
\item You can see all the processes which are controlled by a terminal (aka ``TTY'') with \lstinline{ps}
\item You can see more processes by running \lstinline{ps -x}
\item To see processes from all users (important on clusters), use \lstinline{ps -A}
\item To kill a process, use \lstinline{kill [process id]}
\item To see more information on the running processes, run \texttt{top}
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Break}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Break}
Break
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Programming: C++}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{C Structs}
\begin{frame}[fragile]
\frametitle{C Structs}
\begin{itemize}
\item C allows you to define your own data types as collections of other data types
\item The syntax is strange (and carries over to C++ classes), but there are two main usages. First, to define a structure for one-time usage.
\begin{lstlisting}[language=C++]
struct
{
  int    data1;
  double data2;
} thing;

\end{lstlisting}
Note the semicolon
This a variable \lstinline{thing} which has members
\begin{lstlisting}[language=C++]
thing.data1 = 2;
thing.data2 += 0.1;
\end{lstlisting}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\begin{itemize}
\item More often, structs are used to define repeatedly used data types
\begin{lstlisting}[language=C++]
struct Thing
{
 int data1;
 float data2;
};
struct Thing thing;
\end{lstlisting}
Note the semicolon;
\item C allows you to define alias for datatypes (very useful in C++) 
\begin{lstlisting}[language=C++]
typedef double real
\end{lstlisting}
\item A common use of \lstinline{struct}, to avoid writing the \texttt{struct} keyword, is
\begin{lstlisting}[language=C++]
typedef struct _Thing
{
 int data1;
 float data2;
} Thing;
Thing thing;
\end{lstlisting}
\end{itemize}
\end{frame}

\subsection{Why C++?}
\begin{frame}[fragile]
\frametitle{Why C++?}
\begin{itemize}
\item \textbf{C doesn't scale well to large projects} 
\begin{itemize}
\item Organization requires much discipline
\item Naming clashes are common in practice
\item Concepts like encapsulation, data hiding, modularity, and polymorphism are difficult to enforce.
\end{itemize}
\item C does not include a standard library to hide the error-prone details of memory management and, more generally, use of standard data structures.
\item Easier use of new programming paradigms, notably object-oriented programming.
\item Many more. Generally, C++ does more, but there is more to go wrong.
\end{itemize}
\end{frame}

\subsection{Object-oriented Programming}
\begin{frame}[fragile]
\frametitle{Programming Paradigms}
While on a certain level, all languages and programming approaches do the same things, different languages and organizatonal styles 
prioritize answering different sorts of questions:
\begin{itemize}
\item Procedural Programming. ``What steps need to be taken?''
\item Functional Programming. ``What functions map which inputs to which outputs?''
\item Object-oriented Programming. ``What are the key concepts and how do they interact?''
\item Many more..
\end{itemize}
In practice, all are used to a certain extent. C++ is famous for adding object-oriented features to C (which is typically used in a procedural style), but modern C++ includes many elements associated with functional programming.
\end{frame}

\begin{frame}[fragile]
\frametitle{OOP Concepts}
\begin{itemize}
\item
A key idea in OOP is to maintain a clear separation between an object's \emph{interface} (what is does) and its \emph{implementation} (how it does it).
\item This is powerful conceptually, in terms of letting others using your code, and in terms of rebuilding parts of a project efficiently
\item In C++ source code, this is almost always done by defining \emph{header files} and implementation files which make this separation explicit.
\item Common organization: for every major class (defined in a minute), include a \texttt{.cpp} file and a \texttt{.h} file.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Example}
\begin{itemize}
\item Say we are trying to build an application which solves a PDE. What might be some of the key objects?
\begin{itemize}
\item Solver
\item Matrix
\item Problem Description
\item Domain Description
\item Visualization Context
\item ..
\end{itemize}
\item Excercise: How might you organize these?
\end{itemize}

\end{frame}


\subsection{C++ Classes}
\begin{frame}[fragile]
\frametitle{C++ Classes}
\begin{itemize}
\item An extension of the idea of a C struct
\item Classes contain data and methods
\begin{lstlisting}[language=C++]
class Counter 
{
  public:
    Thing() : count_(0) {} // Initializer syntax for a constructor
    void Increment() { count_ += 1;}
    int GetVal() { return count_; }
    void Reset(); // Not defined
  private:
    int count_;
}; // <-- SEMICOLON
\end{lstlisting}
\item \lstinline{public} and \lstinline{private} refer to whether the data and method are usable from outside the class itself. Typically only a subset of method, called the \emph{public interface} should ever be made \lstinline{public}.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\begin{itemize}
\item Class methods, like other methods, can be declared in one place and defined in another.
\item Class methods like in a namespace (more in a minute) with the name of the class, so I could later define
\begin{lstlisting}[language=C++]
void Counter::Reset() 
{
  count_ = 0;
}
\end{lstlisting}
\end{itemize}
\end{frame}

\subsection{Namespaces}
\begin{frame}[fragile]
\frametitle{Namespaces}
\begin{itemize}
\item The problem: for large projects, especially those with multiple developers, it's likely that you will have naming clashes.
\item Further, if you want to use your code from within another piece of code, you want the names of your functions, classes, etc. to be unique and not clash with any other code.
\item Thus, all variable exist in namespaces in C++.
\item By default, variables are in the \emph{global namespace}
% Name mangling coming up later
\item Declare namespaces as scopes
\begin{lstlisting}[language=C++]
namespace MyNameSpace{
...
}
\end{lstlisting}
\item Import items into the local namespace with \lstinline{using}
\begin{lstlisting}[language=C++]
using std::cout;
using namespace std; //avoid!
\end{lstlisting}

\end{itemize}
\end{frame}

\subsection{References}
\begin{frame}[fragile]
\frametitle{References}
\begin{itemize}
\item References are like immutable pointers in C++
\item They are declared with the \lstinline{&} operator
\begin{lstlisting}[language=C++]
int a;
int &a_ref; //a reference to an int
\end{lstlisting}
\item Since they are immutable, they are rarely declared as above, but are common in function signatures
\begin{lstlisting}[language=C++]
void myFunction(int &a, HugeObject &obj); // do not copy obj when calling, but provide an argument of type HugeObject
\end{lstlisting}
\item They are used like normal variables (no need to dereference)
\end{itemize}
\end{frame}

\subsection{Streams}
\begin{frame}[fragile]
\frametitle{Streams}
\begin{itemize}
\item Streams, as we have seen already with Unix, are a convenient and efficent way to deal with input and output
\item C++ introduces new operators \texttt{<<} and \texttt{>>} for stream usage.
\item These are always one of the first things one sees with C++ tutorials
\begin{lstlisting}[language=C++]
#include<iostream>
...
std::cout << "Hello, World!" << std::endl;
int x;
std::cin >> x;
\end{lstlisting}
\item \texttt{iostream.h} includes \texttt{std::cout} and \texttt{std::cin} for stdin and stdout
\item Streams can also be used for files (\texttt{fstream.h}) and strings (\texttt{sstream.h}).
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Assignment}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\begin{itemize}
\item Suggested Reading : Chapter 2 (``A Tour of C++ : The Basics'') in \emph{The C++ Programming Language} by Stroustrup.
\item Assignment
\begin{itemize}
\item You will design a small C++ project
\item You will construct your own makefile
\item See \texttt{week3/Assignment3.pdf} [will be posted later this afternoon]
\end{itemize}
\end{itemize}
\end{frame}

\end{document}
