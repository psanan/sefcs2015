main : main.o solver.o
	g++ -Wall -lawesome -o main main.o solver.o

main.o : main.cpp
	g++ -Wall -c -o main.o main.cpp

solver.o : solver.cpp
	g++ -Wall -I../awesomelib/include -c -o solver.o solver.cpp

clean :
	rm -f main.o solver.o main

.PHONY : clean
