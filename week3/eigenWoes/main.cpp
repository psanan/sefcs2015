// Adapted from http://eigen.tuxfamily.org/dox/group__TutorialMatrixArithmetic.html

#include <iostream>
#include "Eigen/Dense"

using namespace Eigen;

using std::cout;
using std::endl;

int main(int argc, char *argv[])
{

  Array2d a;
  a << 1, 2,
       3, 4;
  ArrayXd b(2,2);
  b << 2, 3,
       1, 4;
  cout << "a + b =\n" << a + b << std::endl;
  cout << "a - b =\n" << a - b << std::endl;

  return EXIT_SUCCESS;
}
